import React, {Component} from 'react';

// redux dependencies
import { Provider } from 'react-redux'
import { PersistGate } from 'redux-persist/integration/react'
import { store, persistor } from './store'

// own components
// import AppLayout from './src/app'
import LoadingComponent from './src/screens/components/loading-component'

import AppNavigatorWithState from './src/app-navigator-with-state'

type Props = {};
export default class App extends Component<Props> {
  render() {
    return (
      <Provider
        store={store}
      >
      <PersistGate
          loading={<LoadingComponent />}
          persistor={persistor}
        >
          <AppNavigatorWithState />
        </PersistGate>
      </Provider>
    );
  }
}
