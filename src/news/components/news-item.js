import React from 'react'
import {
    View,
    Text,
    Image,
    TouchableWithoutFeedback,
    StyleSheet,
} from 'react-native'

import Avatar from '../../components/avatar'

import moment from 'moment'

const colorOscuro = "#1C1C1C"
const colorRojo = "#ff5500"

function NewsItem(props){
    return(
        <TouchableWithoutFeedback
            onPress={props.onPress}
        >
            <View
                style={styles.container}
            >
                <View
                    style={styles.header}
                >
                    <Avatar 
                        size="small"
                        rounded
                        title={props.author.slice(0,2)}
                        source={{uri: props.authorPhoto}}
                    />
                    <View>
                        <Text
                            style={styles.nombre}
                        >
                            {props.author}
                        </Text>
                        <Text
                            style={styles.cargo}
                        >
                            {props.authorArea}
                        </Text>

                    </View>
                </View>
                <View
                    style={styles.content}
                >
                    <Text
                        style={styles.contentText}
                    >
                        {
                            props.newsDescription ?
                                props.newsDescription
                            :
                                ""
                        }
                    </Text>
                    {
                        props.contentPhoto ?
                            <Image
                                style={styles.contentPhoto}
                                source={{uri: props.contentPhoto}}
                            />
                        :
                            <View></View>
                    }
                </View>
                <View
                    style={styles.pubInfo}
                >
                    {
                        props.likes > 0 ?
                            <Text
                                style={styles.pubInfoText}
                            >
                                {props.likes} likes
                            </Text>
                        :
                            <View></View>
                    }
                    {
                        props.numberOfComments > 0 ?
                        <Text
                            style={styles.pubInfoText}
                        >
                            {props.numberOfComments} comentarios
                        </Text>
                        :
                        <View></View>
                    }
                    <Text
                        style={styles.pubDate}
                    >
                        {moment(props.publishedAt).format('dddd D')} de {moment(props.publishedAt).format('MMMM')}
                    </Text>
                </View>
                <View
                    style={styles.actions}
                >
                    <TouchableWithoutFeedback
                        onPress={props.handleLike.bind(null, props.id)}
                    >
                        <View
                            style={styles.action}
                        >
                            <Text
                                style={props.liked ? styles.buttonTextStyle2 : styles.buttonTextStyle}
                            >
                                me gusta
                            </Text>
                        </View>
                    </TouchableWithoutFeedback>
                    <View
                        style={styles.action}
                    >
                        <Text
                            style={styles.buttonTextStyle}
                        >
                            comentar
                        </Text>
                    </View>
                </View>
            </View>
        </TouchableWithoutFeedback>
    )
}



const styles = StyleSheet.create({
    container:{
        marginTop: 30,
        marginLeft: 10,
        marginRight: 10,
        backgroundColor: '#FFFFFF',
        borderRadius: 10,
        shadowOffset:{
            width: 0,
            height: 0,
        },
        shadowColor: 'black',
        shadowOpacity: 0.4,
        elevation: 1,
    },
    header:{
        flexDirection: 'row',
        alignItems: 'center',
        width: '100%',
        paddingLeft: 5,
        paddingRight: 5,
        paddingTop: 5,
    },
    content:{
        marginTop: 10,
        marginBottom: 10,
    },
    contentText:{
        marginLeft: 15,
        marginRight: 15,
        fontSize: 22,
    },
    contentPhoto:{
        width: '100%',
        height: 400,
        marginTop: 10,
    },
    actions:{
        flexDirection: 'row',
        alignItems: 'center',
        marginLeft: 10,
    },
    pubInfo:{
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingLeft: 10,
        paddingRight: 10,
    },
    pubInfoText:{
        fontSize: 12,
        color: '#757779'
    },
    pubDate:{
        fontSize: 12,
        color: '#757779',
    },
    cargo:{
        fontSize: 12,
        color: '#757779',
        marginLeft: 10,
    },
    action:{
        flexDirection: 'row',
        alignItems: 'center',
        marginRight: 15,
    },
    nombre:{
        fontSize: 18,
        marginLeft: 10,
    },
    buttonTextStyle:{
        fontSize: 16,
        margin: 8,
        color: colorOscuro
    },
    buttonTextStyle2:{
        fontSize: 16,
        margin: 5,
        color: colorRojo
    }
})

export default NewsItem