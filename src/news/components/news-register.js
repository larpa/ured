import React, { Component } from 'react'
import {
    View,
    Text,
    Image,
    TouchableWithoutFeedback,
    TextInput,
    StyleSheet
} from 'react-native'

import { connect } from 'react-redux'

import API from '../../../utils/api'
import showAlert from '../../../utils/alert'
import ImagePicker from '../../../utils/image-picker'

import Avatar from '../../components/avatar'
import Button from '../../components/button'
import ImageAttachBox from '../../components/image-attach-box'


class NewsRegister extends Component{

    state = {
        newsRegisterText: '',
        registerButtonLoading: false,
        files: [],
    }

    handleOnChangeText = (value) => {
        this.setState({
            newsRegisterText: value
        })
    }

    handleAddPhoto = async () => {
        const file = await ImagePicker()
        if(!file.didCancel){
            let bk_files = this.state.files
            bk_files.push(file.uri)
            this.setState({
                files: bk_files
            })
        }
    }

    handleRegisterButton = async () =>{
        if(this.state.newsRegisterText.trim() === ""){
            showAlert('Debes escribir algo.', 'warning', 'long')
            return
        }

        let holderText = this.state.newsRegisterText
        let holderFiles = this.state.files
        this.setState({
            newsRegisterText: '',
            registerLoading: true,
            files: [],
        })

        showAlert('publicando noticia...', 'message', 'long')

        let response = false
        if(holderFiles.length === 0){
            response = await API.registerNew(this.props.user.id, holderText, 'N')
        }else{
            response = await API.registerNewWithPhoto(this.props.user.id, holderText, 'F', holderFiles)
        }

        if(response.success){
            showAlert('Listo!', 'success', 'long')
            this.props.registred()
        }else{
            this.setState({
                newsRegisterText: holderText,
                files: holderFiles
            })
            showAlert('Ocurrio un error publicando la noticia.', 'error', 'long')
        }
        this.setState({
            registerLoading: false
        })
    }

    render(){
        return  (
            <View
                style={styles.container}
            >
                <View
                    style={styles.header}
                >
                    <Avatar 
                        size="medium"
                        rounded
                        title={this.props.user.nombres.slice(0,2)}
                        source={{uri: this.props.user.foto_perfil}}
                    />
                    <Text
                        style={styles.nombre}
                    >
                        {this.props.user.nombres}
                    </Text>
                </View>

                <View
                    style={styles.doubleInput}
                >
                    <TextInput
                        placeholder="Cuentanos de tu día..."
                        style={styles.inputText}
                        multiline={true}
                        onChangeText={this.handleOnChangeText}
                        value={this.state.newsRegisterText}
                    />
                    {
                        this.state.files.length === 0 &&
                        <TouchableWithoutFeedback
                            onPress={this.handleAddPhoto}
                        >
                            <Image
                                source={require('../../../assets/photos.png')}
                                style={styles.addPhotoIcon}
                            />
                        </TouchableWithoutFeedback>
                    }
                </View>
                {
                    this.state.files.length > 0 &&
                                <View
                                    style={styles.imagePreview}
                                >
                                    {
                                        this.state.files.map((value, i) => {
                                            return(
                                                <ImageAttachBox
                                                    key={i}
                                                    onPress={()=>{
                                                        let newFileObject = this.state.files;
                                                        newFileObject.splice(i, 1);
                                                        this.setState({
                                                            files: newFileObject
                                                        })
                                                    }}
                                                    addFile={false}
                                                    source={value}
                                                />
                                            )
                                        })
                                    }
                                </View>
                }

                <Button
                    component
                    style={styles.registerButton}
                    color='#4A3DDD'
                    onPress={this.handleRegisterButton}
                    loading={this.state.registerButtonLoading}
                >
                    <View
                        style={styles.buttonBox}
                    >
                        <Text
                            style={styles.buttonText}
                        >
                            Publicar
                        </Text>
                    </View>
                </Button>

            </View>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        marginTop: 30,
        marginLeft: 10,
        marginRight: 10,
        backgroundColor: '#FFFFFF',
        borderRadius: 10,
        shadowOffset:{
            width: 0,
            height: 0,
        },
        shadowColor: 'black',
        shadowOpacity: 0.4,
        elevation: 1,
    },
    header:{
        flexDirection: 'row',
        alignItems: 'center',
        width: '100%',
        paddingLeft: 5,
        paddingRight: 5,
        paddingTop: 5,

    },
    nombre:{
        fontSize: 18,
        marginLeft: 10,
    },
    inputText:{
        fontSize: 18,
        minHeight: 60,
        maxHeight: 150,
        width: '80%',
        marginLeft: 10,
        marginRight: 10,
        marginTop: 10,
    },
    registerButton:{
        marginLeft: 25,
        marginRight: 25,
        marginTop: 10,
        marginBottom: 10,
    },
    buttonBox:{
		width: '80%',
        marginTop: 10,
        marginLeft: '10%',
        marginBottom: 10,
		padding: 10,
		alignItems: 'center',
		borderRadius: 10,
		backgroundColor: '#4A3DDD',
    },
    buttonText:{
		fontSize: 18,
		color: 'white',
		fontWeight: '600',
    },
    doubleInput:{
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    addPhotoIcon:{
        height: 35,
        width: 30,
        marginRight: 10,
        resizeMode: 'contain'
    },
    imagePreview:{
        marginTop: 10,
        marginLeft: 20,
    }
})

function mapStateToProps(state, props){
    return {
        user: state.user
    }
}

export default connect(mapStateToProps)(NewsRegister)