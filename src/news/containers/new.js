import React, { Component } from 'react'
import {
    View,
    Text,
    Image,
    TouchableWithoutFeedback,
    StyleSheet,
    SafeAreaView,
} from 'react-native'

import { connect } from 'react-redux'

import Avatar from '../../components/avatar'

import moment from 'moment'

const colorOscuro = "#1C1C1C"
const colorRojo = "#ff5500"

class New extends Component{

    render(){
        return(
            <SafeAreaView>
                <View
                    style={styles.container}
                >
                    <View
                        style={styles.header}
                    >
                        <Avatar 
                            size="medium"
                            rounded
                            source={{uri: this.props.newSelected.authorPhoto}}
                        />
                        <View>
                            <Text
                                style={styles.nombre}
                            >
                                {this.props.newSelected.author}
                            </Text>
                            <Text
                                style={styles.cargo}
                            >
                                {this.props.newSelected.authorArea}
                            </Text>

                        </View>
                    </View>
                    <View
                        style={styles.content}
                    >
                        <Text
                            style={styles.contentText}
                        >
                            {
                                this.props.newSelected.newsDescription ?
                                    this.props.newSelected.newsDescription
                                :
                                    ""
                            }
                        </Text>
                        {
                            this.props.newSelected.contentPhoto ?
                                <Image
                                    style={styles.contentPhoto}
                                    source={{uri: this.props.newSelected.contentPhoto}}
                                />
                            :
                                <View></View>
                        }
                    </View>
                    <View
                        style={styles.pubInfo}
                    >
                        {
                            this.props.newSelected.likes > 0 ?
                                <Text
                                    style={styles.pubInfoText}
                                >
                                    {this.props.newSelected.likes} likes
                                </Text>
                            :
                                <View></View>
                        }
                        {
                            this.props.newSelected.numberOfComments > 0 ?
                            <Text
                                style={styles.pubInfoText}
                            >
                                {this.props.newSelected.numberOfComments} comentarios
                            </Text>
                            :
                            <View></View>
                        }
                        <Text
                            style={styles.pubDate}
                        >
                            {moment(this.props.newSelected.publishedAt).format('dddd D')} de {moment(this.props.newSelected.publishedAt).format('MMMM')}
                        </Text>
                    </View>
                    <View
                        style={styles.actions}
                    >
                        <TouchableWithoutFeedback
                            onPress={()=>{
                                // this.props.newSelected.handleLike.bind(null, this.props.newSelected.id)
                            }}
                        >
                            <View
                                style={styles.action}
                            >
                                <Text
                                    style={this.props.newSelected.liked ? styles.buttonTextStyle2 : styles.buttonTextStyle}
                                >
                                    me gusta
                                </Text>
                            </View>
                        </TouchableWithoutFeedback>
                        <TouchableWithoutFeedback
                            onPress={()=>{
                                console.log("comentar publicación")
                            }}
                        >
                            <View
                                style={styles.action}
                            >
                                {/* <Icon
                                    size={20}
                                    name='comment-o'
                                    type='font-awesome'
                                    color={colorOscuro}
                                /> */}

                                <Text
                                    style={styles.buttonTextStyle}
                                >
                                    comentar
                                </Text>
                            </View>
                        </TouchableWithoutFeedback>
                    </View>
                </View>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        // marginTop: 30,
        marginLeft: 10,
        marginRight: 10,
        // backgroundColor: '#FFFFFF',
        // borderRadius: 10,
        // shadowOffset:{
        //     width: 0,
        //     height: 0,
        // },
        // shadowColor: 'black',
        // shadowOpacity: 0.4,
        // elevation: 1,
        height: '100%'
    },
    header:{
        flexDirection: 'row',
        alignItems: 'center',
        width: '100%',
        paddingLeft: 5,
        paddingRight: 5,
        paddingTop: 5,

    },
    content:{
        marginTop: 10,
        marginBottom: 10,
    },
    contentText:{
        marginLeft: 15,
        marginRight: 15,
        fontSize: 22,
    },
    contentPhoto:{
        width: '100%',
        height: 300,
        marginTop: 10,
    },
    actions:{
        flexDirection: 'row',
        alignItems: 'center',
        marginLeft: 10,
    },
    pubInfo:{
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingLeft: 10,
        paddingRight: 10,
    },
    pubInfoText:{
        fontSize: 12,
        color: '#757779'
    },
    pubDate:{
        fontSize: 12,
        color: '#757779',
    },
    cargo:{
        fontSize: 12,
        color: '#757779',
        marginLeft: 10,
    },
    action:{
        flexDirection: 'row',
        alignItems: 'center',
        marginRight: 15,
    },
    nombre:{
        fontSize: 18,
        marginLeft: 10,
    },
    buttonTextStyle:{
        fontSize: 16,
        margin: 8,
        color: colorOscuro
    },
    buttonTextStyle2:{
        fontSize: 16,
        margin: 5,
        color: colorRojo
    }
})

function mapStateToProps(state, props){
    return {
        newSelected: state.news.newSelected
    }
}

export default connect(mapStateToProps)(New)