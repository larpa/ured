import React, { Component } from 'react'
import {
    View,
    Text,
    FlatList,
    SafeAreaView,
    RefreshControl,
    StyleSheet,
} from 'react-native'

import { connect } from 'react-redux'

import { NavigationActions } from 'react-navigation'

import NewItem from '../components/news-item'
import EmptyNewsList from '../components/empty-news-list'
import NewsRegister from '../components/news-register'
import hepaticFeedback from '../../../utils/hepatic-feedback'

import API from '../../../utils/api'

class NewsList extends Component{

    state = {
        refreshing: false
    }

    keyExtractor = item => item.id.toString()
    renderEmptyComponent = () => <EmptyNewsList
                                    text="Aún no hay Noticias"
                                />
    renderFirstComponent = () => <NewsRegister
                                    registred={this.handleRegistred}
                                />
    renderItem = ({item}) => <NewItem
                                {...item}
                                handleLike={this.handleLike}
                                onPress={this.handleSingleEvent.bind(null, item.id)}
                            />
    renderLastItem = () => <View style={{ height: 0, marginBottom: 150}} ></View>

    handleRegistred = () => {
        this.loadNewsList()
    }

    handleSingleEvent = (id) =>{
        // Lo siguiente es para guardar en el state que publicacion se esta seleccionando
        this.props.dispatch({
            type: "SELECT_NEW_LIST",
            payload:{
                id
            }
        })
        // Lo siguiente es para entrar a la vista de una publicación (donde ya deberian estar los comentarios)
        this.props.dispatch(NavigationActions.navigate({
            routeName: 'New',
            params: {
                id_selected: id
            }
        }))
    }

    handleRefresh = () => {
        this.loadNewsList()
    }

    componentDidMount = () => {
        this.loadNewsList()
    }

    loadNewsList = async () => {
        this.setState({
            refreshing: true
        })
        if(this.props.user.id){
            const NewsData = await API.requestNews(this.props.user.id, 0)
            if(NewsData.success){
                this.props.dispatch({
                    type: 'LOAD_NEWS_DATA',
                    payload: {
                        news: NewsData.response
                    }
                })
            }
        }
        this.setState({
            refreshing: false
        })
    }

    handleLike = async (pub_id) => {
        if(this.props.user.id){

            hepaticFeedback('impactHeavy')

            let newLiked = !this.props.news.find((item)=> item.id === pub_id).liked

            this.props.dispatch({
                type: 'LIKE_NEW',
                payload: {
                    newUpdated: {
                        ...this.props.news.find((item)=> item.id === pub_id),
                        liked: newLiked
                    }
                }
            })

            const liked = await API.likeNew(pub_id, this.props.user.id)

            if(liked.success){
                this.props.dispatch({
                    type: 'LIKE_NEW',
                    payload: {
                        newUpdated: liked.response[0]
                    }
                })
            }

        }
    }

    render(){
        return (
            <SafeAreaView>
                <View>
                    <View
                        style={styles.header}
                    >
                        <Text
                            style={styles.title}
                        >
                            Noticias
                        </Text>
                    </View>

                    <FlatList
                        style={styles.list}
                        keyExtractor={this.keyExtractor}
                        data={this.props.news}
                        ListEmptyComponent={this.renderEmptyComponent}
                        renderItem={this.renderItem}
                        ListHeaderComponent={this.renderFirstComponent}
                        ListFooterComponent={this.renderLastItem}
                        refreshControl={<RefreshControl refreshing={this.state.refreshing} onRefresh={this.handleRefresh}/>}
                    />
                </View>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    list:{
        height: '100%'
    },
    header: {
        width: '100%',
        alignItems: 'center',
        marginTop: 15,
    },
    title:{
        fontSize: 24,
        fontWeight: '300',
    }
})

function maptStateToProps(state, props){
    let holderNews = []
    if(state.news){
        holderNews = state.news.newsList
    }
    return {
        news: holderNews,
        user: state.user
    }
}

export default connect(maptStateToProps)(NewsList)