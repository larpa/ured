import React, { Component } from 'react'
import {
    View,
    ScrollView,
    SafeAreaView,
    TouchableOpacity,
    Text,
    StyleSheet
} from 'react-native'
import { NavigationActions } from 'react-navigation'

import { connect } from 'react-redux'
import Button from '../../components/button';

class EventList extends Component{
    handleLogOut = () =>{
        this.props.dispatch({
            type: "CLEAR_NOTIFICATION",
        })
        this.props.dispatch({
            type: "CLEAR_NEWS",
        })
        this.props.dispatch({
            type: "CLEAR_SOLICITUDES",
        })
        this.props.dispatch({
            type: "CLEAR_MY_NEWS",
        })
        this.props.dispatch({
            type: "CLEAR_MENUS",
        })
        this.props.dispatch({
            type: "CLEAR_COLABORADORES",
        })
        this.props.dispatch({
            type: "LOG_OUT",
        })
        this.props.dispatch(NavigationActions.navigate({
            routeName: 'Login'
        }))
    }

    handleOpenMisSolicitudes = () => {
        this.props.dispatch(NavigationActions.navigate({
            routeName: 'MisSolicitudes'
        }))
    }

    handleOpenSolicitudes = () => {
        this.props.dispatch(NavigationActions.navigate({
            routeName: 'Solicitudes'
        }))
    }

    handleOpenContactos = () => {
        this.props.dispatch(NavigationActions.navigate({
            routeName: 'Contactos'
        }))
    }

    handleOpenCafeta = () => {
        this.props.dispatch(NavigationActions.navigate({
            routeName: 'MenuList'
        }))
    }

    handleOpenErrorList = () => {
        this.props.dispatch(NavigationActions.navigate({
            routeName: 'ErrorList'
        }))
    }

    handleOpenNewError = () => {
        this.props.dispatch(NavigationActions.navigate({
            routeName: 'AddError'
        }))
    }

    handleOpenColabMes = () => {
        this.props.dispatch(NavigationActions.navigate({
            routeName: 'ColaboradorMesList'
        }))
    }

    render(){
        return(
            <SafeAreaView>
                <ScrollView
                    style={styles.container}
                >
                    <TouchableOpacity
                        style={styles.itemMenuBox}
                        onPress={this.handleOpenSolicitudes}
                    >
                        <Text style={styles.itemMenuText}>
                            Solicitudes
                        </Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={styles.itemMenuBox}
                        onPress={this.handleOpenCafeta}
                    >
                        <Text style={styles.itemMenuText}>
                            Cafeta
                        </Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={styles.itemMenuBox}
                        onPress={this.handleOpenMisSolicitudes}
                    >
                        <Text style={styles.itemMenuText}>
                            Mis solicitudes
                        </Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={styles.itemMenuBox}
                        onPress={this.handleOpenContactos}
                    >
                        <Text style={styles.itemMenuText}>
                            Contactos
                        </Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={styles.itemMenuBox}
                        onPress={this.handleOpenColabMes}
                    >
                        <Text style={styles.itemMenuText}>
                            Colaborador del mes
                        </Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={styles.itemMenuBox}
                        onPress={this.handleOpenNewError}
                    >
                        <Text style={styles.itemMenuText}>
                            Reporta una queja
                        </Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={styles.itemMenuBox}
                        onPress={this.handleOpenErrorList}
                    >
                        <Text style={styles.itemMenuText}>
                            Lista de quejas
                        </Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={styles.itemMenuBox}
                        onPress={this.handleLogOut}
                    >
                        <Text style={styles.itemMenuText}>
                            Cerrar sesión
                        </Text>
                    </TouchableOpacity>
                </ScrollView>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        height: '100%'
    },
    itemMenuBox:{
        marginTop: 20,
        paddingBottom: 20,
        paddingLeft: 20,
        borderBottomWidth: 1,
        borderBottomColor: '#999999'
    },
    itemMenuText:{
        fontSize: 30,
        fontWeight: '200',
    }
})

function mapStateToProps(state, props){
    return{
        user: state.user
    }
}

export default connect(mapStateToProps)(EventList)