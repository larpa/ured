import React from 'react'
import {
    SafeAreaView
} from 'react-native'

function ProfileLayout(props){
    return(
        <SafeAreaView>
            {props.children}
        </SafeAreaView>
    )
}

export default ProfileLayout