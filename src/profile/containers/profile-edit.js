import React, { Component } from 'react'
import {
    Text,
    View,
    TouchableOpacity,
    ScrollView,
    StyleSheet,
} from 'react-native'
import {connect} from 'react-redux'

// Own components
import Button from '../../components/button'
import Header from '../../components/header'
import TextAreaInput from '../../components/text-area-input'
import TextInput from '../../components/text-input'
import InputDate from '../../components/text-input-date'
import ImageCirle from '../../components/image-circle'
import PickerCiudades from '../../components/picker-ciudades'
import PickerSexo from '../../components/picker-sexo'
import ProfileLayout from '../components/profile-layout'
import Loader from '../../components/loader'
import showAlert from '../../../utils/alert'
import imagePicker from '../../../utils/image-picker'

// external components
import DatePicker from 'react-native-datepicker'

// utilities
import Dimensions from 'Dimensions'
import API from '../../../utils/api'
import { NavigationActions } from 'react-navigation'
import moment from 'moment'
import 'moment/locale/es'

const { width, height } = Dimensions.get('window')

class ProfileEdit extends Component{
    state = {
        user: {},
        isLoading: false,
        isImageLoading: false,
    }

    handleOnChangeDay = (text) => {
        this.setState({
            day: text
        })
    }

    handleOnChangeMonth = (text) => {
        this.setState({
            month: text
        })
    }

    handleOnChangeYear = (text) => {
        this.setState({
            year: text
        })
    }

    handleOnChangeDescription = (text) => {
        this.setState({
            user: {
                ...this.state.user,
                descripcion: text
            }
        })
    }

    handleOnChangeTelefono = (text) => {
        this.setState({
            user: {
                ...this.state.user,
                telefono: text
            }
        })
    }
    
    handleOnChangeNumeroDocumento = (text) => {
        this.setState({
            user: {
                ...this.state.user,
                numero_documento: text
            }
        })
    }

    handleOnChangeCiudad = (value) => {
        this.setState({
            user: {
                ...this.state.user,
                id_ciudad: value
            }
        })
    }

    handleOnChangeSexo = (value) => {
        this.setState({
            user: {
                ...this.state.user,
                sexo: value
            }
        })
    }

    handleOnChangeFechaNacimiento = (value) => {
        this.setState({
            user: {
                ...this.state.user,
                fecha_nacimiento: value
            }
        })
    }

    componentWillMount = () => {
      this.setState({
          user: this.props.user
      })
    }
    
    handleOnPressProfilePicture = async () => {
        const imagePickerResponse = await imagePicker(true)
        if(!imagePickerResponse.didCancel){
            this.setState({
                isImageLoading: true
            })
            const newProfileUrlResponse = await API.uploadProfilePic(this.props.user.id, imagePickerResponse)
            this.setState({
                isImageLoading: false
            })
            if(newProfileUrlResponse.success){
                this.props.dispatch({
                    type: "SET_USER",
                    payload:{
                        user: [{
                            ...this.props.user,
                            foto_perfil: newProfileUrlResponse.response
                        }]
                    }
                })
                showAlert("Foto de perfil actualizada", "message", 'long')
            }else{
                showAlert("Ocurrio un problema al subir tu foto.", "error", 'long')
            }
        }
    }

    updateProfile = async () => {

        const id = this.state.user.id
        const descripcion = this.state.user.descripcion  ? this.state.user.descripcion : '';
        const fecha_nacimiento = this.state.user.fecha_nacimiento ? moment(this.state.user.fecha_nacimiento).format('YYYY-MM-DD') : '';
        const numero_documento = this.state.user.numero_documento ? this.state.user.numero_documento : '';
        const telefono = this.state.user.telefono ? this.state.user.telefono : '';
        const id_ciudad = this.state.user.id_ciudad ? this.state.user.id_ciudad : '';
        const sexo = this.state.user.sexo ? this.state.user.sexo : '';

        if(numero_documento.trim().length > 1 && numero_documento.trim().length < 8){
            showAlert('El número de documento debe tener minimo 8 digitos', 'warning', 'short')
            return
        }
        if(telefono.trim().length > 0 && telefono.trim().length < 6){
            showAlert('Necesitamos un telefono valido', 'warning', 'short')
            return
        }

        this.setState({
            isLoading: true
        })

        const userData = await API.updateProfileInfo(id, descripcion, fecha_nacimiento, numero_documento, telefono, id_ciudad, sexo)
        this.setState({
            isLoading: false
        })
        if(userData.success){
            this.props.dispatch({
                type: "SET_USER",
                payload:{
                    user: userData.data
                }
            })
            showAlert('Perfil actualizado', 'message', 'long')
            setTimeout(()=>{
                this.props.dispatch(NavigationActions.navigate({
                    routeName: 'Profile'
                }))
            }, 500)
        }
    }

    render(){
        return(
            <ProfileLayout>
                <Header 
                    title="Edita tus datos"
                />
                <ScrollView>
                    <Loader 
						visible={this.state.isLoading}
					/>
                    <View style={styles.container}>
                            <ImageCirle 
                                loading={this.state.isImageLoading}
                                editView={true}
                                url={this.props.user.foto_perfil ? this.props.user.foto_perfil : ''}
                                onPress={this.handleOnPressProfilePicture}
                            />
                            <Text 
                                style={styles.name}
                            >
                                {`${this.props.user.nombre} ${this.props.user.apellido}`}
                            </Text>
                            <TextAreaInput
                                style={styles.description}
                                numberOfLines={4}
                                placeholder="Escribe una descripción acerca de tí ..."
                                onChangeText={this.handleOnChangeDescription}
                                value={this.state.user.descripcion}    
                                blurOnSubmit={true}
                                returnKeyType="next"
                            />
                            <DatePicker
                                style={styles.birthDay}
                                customStyles={dateInputStyles}
                                date={this.state.user.fecha_nacimiento}
                                mode="date"
                                locale={'es'}
                                placeholder="Fecha nacimiento"
                                format="YYYY-MM-DD"
                                minDate="1919-01-01"
                                maxDate={moment().format("YYYY-MM-DD")}
                                confirmBtnText="Ok"
                                cancelBtnText="Cancelar"
                                showIcon={false}
                                onDateChange={this.handleOnChangeFechaNacimiento}
                            />
                           {
                                this.props.user.numero_documento ?
                                    <View></View>
                                :
                                    <TextInput
                                        style={styles.inputText}
                                        placeholder="Número de documento"
                                        keyboardType="number-pad"
                                        onChangeText={this.handleOnChangeNumeroDocumento}
                                        value={this.state.user.numero_documento}
                                        returnKeyType="next"
                                        setRef={element => this.txtNumeroDocumento = element}
                                    />
                           }
                            <TextInput
                                style={styles.inputText}
                                placeholder="Telefono"
                                keyboardType="phone-pad"
                                onChangeText={this.handleOnChangeTelefono}
                                value={this.state.user.telefono}
                                returnKeyType="next"
                                setRef={element => this.txtNumeroDocumento = element}
                            />
                            <View
                                style={styles.infoContainer}
                            >
                                <PickerCiudades
                                    onValueChange={this.handleOnChangeCiudad}
                                    value={this.state.user.id_ciudad}
                                />
                            </View> 
                            <View
                                style={styles.infoContainer}
                            >
                                <PickerSexo
                                    onValueChange={this.handleOnChangeSexo}
                                    value={this.state.user.sexo}
                                />
                            </View> 
                            <Button 
                                style={styles.submitButton}
                                text="GUARDAR" 
                                onPress={this.updateProfile}
                            />
                        </View>
                </ScrollView>
            </ProfileLayout>
        )
    }
}

const dateInputStyles = {
    dateInput:{
        borderLeftWidth: 0,
        borderRightWidth: 0,
        borderTopWidth: 0,
        borderBottomWidth: 0.7,
        borderBottomColor: '#999999',
        marginBottom: 30,
        alignItems: 'flex-start',
    },
    placeholderText:{
        fontSize: 20,
    },

}

const styles = StyleSheet.create({
    container:{
        padding: 25,
        marginBottom: 50,
        alignItems: 'center',
		justifyContent: 'center',
    },
    dates:{
        flexDirection: 'row'
    },
    infoContainer: {
        textAlign: 'left',
        width: width/1.2,
        marginBottom: 18,
    },
    name:{
        marginTop: 15,
        fontSize: 35,
        fontWeight: '300'
    },
    description:{
        marginTop: 20,
        fontSize: 14,
        color: 'black',
        minHeight: 50,
        maxHeight: 80,
        width:width/1.2,
        borderBottomWidth:0.7,
    },
    submitButton:{
        marginTop: 10,
        width: width/1.1
    },
    inputText:{
        fontSize: 18,
        width: width/1.2,
        borderBottomWidth: 0.7
    },
    birthDay:{
        borderWidth: 0,
        borderColor: 'white',
        width: width/1.2,
        marginBottom: 10,
    },
})

function mapStateToProps(state, props){
    return{
        user: state.user[0]
    }
}

export default connect(mapStateToProps)(ProfileEdit)