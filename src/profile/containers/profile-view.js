import React, { Component } from 'react'
import {
    Text,
    View,
    SafeAreaView,
    StyleSheet,
    RefreshControl,
    TouchableWithoutFeedback,
    ScrollView,
    Image,
    FlatList
} from 'react-native'

import Avatar from '../../components/avatar'

import { connect } from 'react-redux'

import { NavigationActions } from 'react-navigation'

// Own components
import NewItem from '../../news/components/news-item'
import EmptyNewsList from '../../news/components/empty-news-list'
import Separator from '../../components/separator'
import hepaticFeedback from '../../../utils/hepatic-feedback'

import API from '../../../utils/api'
import ImagePicker from '../../../utils/image-picker'

// utilities
import Dimensions from 'Dimensions'

const {
	width,
	height
} = Dimensions.get('window')

class ProfileView extends Component{
    state = {
        refreshing: false,
        files: []
    }

    keyExtractor = item => item.id.toString()
    renderEmptyComponent = () => <EmptyNewsList
                                    text="Aun no hay Noticias"
                                />
    renderItem = ({item}) => <NewItem
                                {...item}
                                handleLike={this.handleLike}
                                onPress={this.handleSingleEvent.bind(null, item.id)}
                            />
    renderLastItem = () => <View style={{ height: 0, marginBottom: 250}} ></View>

    handleLike = async (pub_id) => {
        if(this.props.user.id){

            hepaticFeedback('impactHeavy')

            let newLiked = !this.props.myNewsList.find((item)=> item.id === pub_id).liked

            this.props.dispatch({
                type: 'LIKE_MY_NEW',
                payload: {
                    newUpdated: {
                        ...this.props.myNewsList.find((item)=> item.id === pub_id),
                        liked: newLiked
                    }
                }
            })

            const liked = await API.likeNew(pub_id, this.props.user.id)

            if(liked.success){
                this.props.dispatch({
                    type: 'LIKE_MY_NEW',
                    payload: {
                        newUpdated: liked.response[0]
                    }
                })
            }

        }
    }

    handleSingleEvent = (id) =>{
        // Lo siguiente es para guardar en el state que publicacion se esta seleccionando
        this.props.dispatch({
            type: "SELECT_NEW_LIST",
            payload:{
                id
            }
        })
        // Lo siguiente es para entrar a la vista de una publicación (donde ya deberian estar los comentarios)
        this.props.dispatch(NavigationActions.navigate({
            routeName: 'New',
            params: {
                id_selected: id
            }
        }))
    }


    loadNewsList = async () => {
        this.setState({
            refreshing: true
        })
        if(this.props.user.id){
            const NewsData = await API.requestNews(this.props.user.id, this.props.user.id)
            if(NewsData.success){
                this.props.dispatch({
                    type: 'LOAD_MY_NEWS_DATA',
                    payload: {
                        news: NewsData.response
                    }
                })
            }
        }
        this.setState({
            refreshing: false
        })
    }

    handleRefresh = () => {
        this.loadNewsList()
    }

    handleChangeProfilePic = async () => {
        console.log('triggered change profile pic')

        const file = await ImagePicker(true)
        if(!file.didCancel){
            const response = await API.changeProfilePic(this.props.user.id, file.uri)
            if(response.success){
                console.log('profile updated')
                this.props.dispatch({
                    type: 'SET_USER',
                    payload: {
                        user: {
                            ...this.props.user,
                            foto_perfil: response.response
                        }
                    }
                })
            }
        }
    }

    componentDidMount() {
        this.loadNewsList()
    }

    render(){
        return(
            <SafeAreaView>
                <ScrollView
                    refreshControl={<RefreshControl refreshing={this.state.refreshing} onRefresh={this.handleRefresh}/>}
                >
                    <View style={styles.container}>
                        <View
                            style={styles.importantDataBox}
                        >
                            <Avatar 
                                size="medium"
                                rounded
                                title={this.props.user.nombres.slice(0,1) + this.props.user.apellido_paterno.slice(0,1)}
                                source={this.props.user.foto_perfil ? {uri: this.props.user.foto_perfil} : {}}
                                onPress={this.handleChangeProfilePic}
                            />
                            <Text
                                style={styles.name}
                            >
                                {`${this.props.user.nombres} ${this.props.user.apellido_paterno}`}
                            </Text>
                        </View>
                        {
                           this.props.user.puesto ?
                                <View
                                    style={styles.infoBox}
                                >
                                    <Image
                                        source={require('../../../assets/identificacion.png')}
                                        style={styles.infoIcon}
                                    />
                                    <Text
                                        style={styles.infoLabel}
                                    >
                                        {this.props.user.puesto}
                                    </Text>
                                </View>
                            :
                                <View></View>
                        }

                        {
                            this.props.user.correo ?
                                <View
                                    style={styles.infoBox}
                                >
                                    <Image
                                        source={require('../../../assets/mail.png')}
                                        style={styles.infoIcon}
                                    />
                                    <Text
                                        style={styles.infoLabel}
                                    >
                                        {this.props.user.correo}
                                    </Text>
                                </View>
                            :
                                <View></View>
                        }
                        {
                            this.props.user.telefono1 ?
                                <View
                                    style={styles.infoBox}
                                >
                                    <Image
                                        source={require('../../../assets/mobile.png')}
                                        style={styles.infoIcon}
                                    />
                                    <Text
                                        style={styles.infoLabel}
                                    >
                                        {this.props.user.telefono1}
                                    </Text>
                                </View>
                            :
                                <View></View>
                        }
                        {
                            this.props.user.telefono2 ?
                                <View
                                    style={styles.infoBox}
                                >
                                    <Image
                                        source={require('../../../assets/mobile.png')}
                                        style={styles.infoIcon}
                                    />
                                    <Text
                                        style={styles.infoLabel}
                                    >
                                        {this.props.user.telefono2}
                                    </Text>
                                </View>
                            :
                                <View></View>
                        }
                    </View>
                    <Separator />
                    <View
                        style={styles.noticiasBox}
                    >
                        <FlatList
                            style={styles.list}
                            keyExtractor={this.keyExtractor}
                            data={this.props.myNewsList}
                            ListEmptyComponent={this.renderEmptyComponent}
                            renderItem={this.renderItem}
                            ListFooterComponent={this.renderLastItem}
                            // refreshControl={<RefreshControl refreshing={this.state.refreshing} onRefresh={this.handleRefresh}/>}
                        />
                    </View>

                </ScrollView>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        padding: 15,
        paddingTop: 30,
        // alignItems: 'center',
        // height: height + height*0.1,
    },
    importantDataBox:{
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
    name:{
        marginLeft: 15,
        fontSize: 30,
        fontWeight: '300'
    },
    dates:{
        flexDirection: 'row'
    },
    infoBox:{
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 10,
        marginLeft: 10
    },
    infoLabel:{
        marginLeft: 20,
        fontSize: 18,
        fontWeight: '300',
        color: '#4B4B4B',
    },
    mail: {
        marginTop: 10,
        fontSize: 20,
        color: '#757779',
        fontWeight: '300'
    },
    descriptionText:{
        marginTop: 20,
        fontSize: 16,
        color: '#757779',
        width:width/1.2,
        marginBottom: 15,
    },
    slash:{
        marginTop: 10,
        fontSize: 18,
    },
    edit:{
        height: 22,
        width: 22,
        resizeMode: 'contain',
    },
    editButton:{
        height: 30,
        width: 100,
        flexDirection: 'row-reverse',
    },
    infoContainer: {
        textAlign: 'left',
        width: width/1.2,
        marginTop: 10,
    },
    infoContainerLabel: {
        fontSize: 14,
        marginBottom: 5
    },
    infoContainerValue: {
        fontSize: 18
    },
    noticiasBox:{

    },
    infoIcon:{
        height: 25,
        width: 25,
        resizeMode: 'contain'
    }
})

function mapStateToProps(state, props){
    console.log("HERE: ", state.myNews.myNewsList)
    return{
        user: state.user,
        myNewsList: state.myNews.myNewsList
    }
}

export default connect(mapStateToProps)(ProfileView)