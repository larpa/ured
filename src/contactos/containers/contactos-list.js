import React, {Component} from 'react'
import {
    View,
    Text,
    SafeAreaView,
    FlatList,
    RefreshControl,
    Linking,
    StyleSheet
} from 'react-native'

import { connect } from 'react-redux'
import { NavigationActions } from 'react-navigation'

import ActionButton from 'react-native-action-button'

import Header from '../../components/header'
import Search from '../../components/search'

import API from '../../../utils/api'
import showAlert from '../../../utils/alert'

import Contacto from '../components/contacto'
import EmptyContactoList from '../components/empty-contacto-list'

class ContactosList extends Component {

    state = {
        refreshing: false,
        list: [],
        searchValue: ''
    }

    keyExtractor = item => item.id.toString()

    renderEmptyComponent = () => <EmptyContactoList
                                    text="No se encontraron contactos."
                                />

    renderLastItem = () => <View style={styles.emptyItem}></View>

    renderItem = ({item}) => <Contacto
                                {...item}
                                onPress={this.handleCall}
                            />

    handleCall = (telefono) => {
        console.log(telefono)
        Linking.openURL(`tel:${telefono}`)
    }

    handleOnBack = () => {
        this.props.dispatch(NavigationActions.back())
    }

    handleRefresh = () => {
        this.handleLoadList()
    }

    componentDidMount = () => {
        this.setState({
            list: this.props.contactos
        })
        this.handleLoadList()
    }

    handleLoadList = async () => {
        this.setState({
            refreshing: true
        })
        const response = await API.getContactos()
        if(response.success){
            this.props.dispatch({
                type: 'LOAD_CONTACTOS_LIST_DATA',
                payload: {
                    contactos: response.response
                }
            })
        }
        this.setState({
            refreshing: false
        })
    }

    handleChangeSearch = value => {
        this.setState({
            searchValue: value
        })
        let holder = [];

        this.props.contactos.map(item => {
            if(item.nombre.includes(value)){
                holder.push(item)
            }
        })

        this.setState({
            list: holder
        })

    }

    render(){
        return(
            <SafeAreaView>
                <View
                    style={styles.container}
                >
                    <Header
                        title="Contactos"
                        back
                        onPress={this.handleOnBack}
                    />
                    <Search
                        placeholder="Buscar"
                        style={styles.serarch}
                        text={this.state.searchValue}
                        onChange={ this.handleChangeSearch }
                    />
                    <FlatList
                        style={styles.list}
                        keyExtractor={this.keyExtractor}
                        data={this.state.list}
                        ListEmptyComponent={this.renderEmptyComponent}
                        renderItem={this.renderItem}
                        ListFooterComponent={this.renderLastItem}
                        refreshControl={
                            <RefreshControl
                                refreshing={this.state.refreshing}
                                onRefresh={this.handleRefresh}
                            />
                        }
                    />
                </View>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        height: '100%',
    },
    list:{
        height: '100%'
    },
    emptyItem:{
        height: 0,
        marginBottom: 150
    },
    serarch:{
        marginTop: 10,
    }
})

function mapStateToProps(state, props){
    return{
        user: state.user,
        contactos: state.contactos.contactosList
    }
}

export default connect(mapStateToProps)(ContactosList)