import React from 'react'
import {
    View,
    Text,
    Image,
    TouchableWithoutFeedback,
    StyleSheet,
} from 'react-native'

function Contacto(props){
    return(
        <TouchableWithoutFeedback
            onPress={props.onPress.bind(null, props.telefono)}
        >
            <View
                style={styles.containter}
            >
                <Image
                    style={styles.image}
                    source={{uri: props.foto_perfil}}
                />
                <View>
                    <Text
                        style={styles.nombre}
                    >
                        {props.nombre}
                    </Text>
                    <Text
                        style={styles.area}
                    >
                        {props.area}
                    </Text>
                    <Text
                        style={styles.correo}
                    >
                        {props.correo}
                    </Text>
                    {
                        props.telefono !== '-' &&
                        <Text
                            style={styles.telefono}
                        >
                            {props.telefono}
                        </Text>
                    }
                </View>
            </View>
        </TouchableWithoutFeedback>
    )
}

const styles = StyleSheet.create({
    containter: {
        flexDirection: 'row',
        backgroundColor: '#FFFFFF',
        borderRadius: 5,
        shadowOffset:{
            width: 0,
            height: 0,
        },
        shadowColor: 'black',
        shadowOpacity: 0.4,
        elevation: 1,
        marginTop: 25,
        marginLeft: 10,
        marginRight: 10,
        padding: 10,
    },
    image: {
        height: 50,
        width: 50,
        borderRadius: 25,
    },
    nombre: {
        fontSize: 22,
        marginLeft: 15,
    },
    area: {
        fontSize: 14,
        color: '#514E4A',
        marginTop: 5,
        marginLeft: 20,
    },
    correo: {
        fontSize: 14,
        marginTop: 5,
        marginLeft: 20,
    },
    telefono: {
        fontSize: 14,
        marginTop: 5,
        marginLeft: 20,
    }
})

export default Contacto