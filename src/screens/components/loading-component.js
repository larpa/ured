import React from 'react'
import {
  View,
  Image,
  Text,
  StyleSheet,
  ActivityIndicator,
} from 'react-native'

function LoadingContainer(props) {
  return (
    <View style={styles.container}>

      <Text style={styles.title}>URED</Text>
      <Text style={styles.text}>Cargando...</Text>
      <ActivityIndicator />
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    alignItems: 'center',
    justifyContent: 'center',
  },
  title:{
    fontSize: 30,
    color: 'red',
  },
  logo: {
    width: 200,
    height: 100,
    resizeMode: 'contain',
    marginBottom: 10,
  },
  text:{
      margin: 10
  }
})


export default LoadingContainer