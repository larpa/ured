import React, { Component } from 'react'
import { connect } from 'react-redux'

import LoadingComponent from '../components/loading-component'

class Loading extends Component{
    componentDidMount(){
        if(!this.props.user){
            this.props.navigation.navigate('Login')
        }else{
            this.props.navigation.navigate('App')
        }
    }

    render(){
        return (
            <LoadingComponent />
        )
    }
}

function mapStateToProps(state, props){
    return{
        user: state.user
    }
}


export default connect(mapStateToProps)(Loading)