import React, { Component } from 'react'

import Login from './screens/containers/login'
import LoginForm from './login/containers/login-form'

class App extends Component{
    render() {
        return (
          <Login>
            <LoginForm />
          </Login>
        );
      }
}

export default App
