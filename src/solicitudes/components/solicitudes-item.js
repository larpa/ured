import React from 'react'
import {
    View,
    Text,
    TouchableWithoutFeedback,
    StyleSheet,
} from 'react-native'

import Avatar from '../../components/avatar'

import moment from 'moment'

function SolicitudesItem(props){
    return(
        <TouchableWithoutFeedback
            onPress={props.onPress}
        >
            <View
                style={styles.container}
            >
                <View
                    style={styles.innerContainer}
                >
                    <View
                        style={styles.doubleView}
                    >
                        <Text
                            style={styles.topLabel}
                        >
                            {props.id}
                        </Text>
                        <Text
                            style={styles.topLabel}
                        >
                            {moment(props.timestamp_registro).format("DD/MM/YYYY")}
                        </Text>
                    </View>
                    <View
                        style={styles.header}
                    >
                        <Avatar 
                            size="medium"
                            rounded
                            title={props.solicitante.slice(0,2)}
                            source={{uri: props.foto_solicitante}}
                        />
                        <View>
                            <Text
                                style={styles.nombre}
                            >
                                {props.solicitante}
                            </Text>
                        </View>
                    </View>
                    <Text
                        style={styles.titleLabel}
                    >
                        {props.tipo_solicitud}
                    </Text>
                    <Text
                        style={styles.motivoLabel}
                    >
                        {props.motivo}
                    </Text>
                    <View
                        style={styles.doubleView}
                    >
                        <Text
                            style={styles.bottomLabel}
                        >
                            {props.urgente == 'U' ? "URGENTE" : "NORMAL"}
                        </Text>
                        <Text
                            style={styles.bottomLabel}
                        >
                            {props.descripcion_estado.toUpperCase()}
                        </Text>
                    </View>
                </View>
                {
                    props.estado === 'G' &&
                        <View style={styles.black}></View>
                }
                {
                    props.estado === 'E' &&
                        <View style={styles.black}></View>
                }
                {
                    props.estado === 'D' &&
                        <View style={styles.yellow}></View>
                }
                {
                    props.estado === 'P' &&
                        <View style={styles.green}></View>
                }
                {
                    props.estado === 'R' &&
                        <View style={styles.red}></View>
                }
            </View>
        </TouchableWithoutFeedback>
    )
}

const styles = StyleSheet.create({
    container:{
        backgroundColor: '#FFFFFF',
        borderRadius: 5,
        shadowOffset:{
            width: 0,
            height: 0,
        },
        shadowColor: 'black',
        shadowOpacity: 0.4,
        elevation: 1,
        marginTop: 25,
        marginLeft: 10,
        marginRight: 10,
    },
    innerContainer:{
        padding: 10,
    },
    doubleView:{
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    topLabel:{
        fontSize: 20,
        fontWeight: '300',
        color: '#757779'
    },
    titleLabel: {
        fontSize: 22,
        marginTop: 10,
    },
    motivoLabel: {
        fontSize: 18,
        fontWeight: '400',
        color: '#757779',
        marginLeft: 15,
        marginBottom: 10,
    },
    bottomLabel:{
        fontSize: 12,
        fontWeight: 'bold',
    },
    black: {
        width: '100%',
        height: 10,
        backgroundColor: '#1C1C1C',
        borderBottomLeftRadius: 5,
        borderBottomRightRadius: 5,
    },
    yellow: {
        width: '100%',
        height: 10,
        backgroundColor: '#DFFF13',
        borderBottomLeftRadius: 5,
        borderBottomRightRadius: 5,
    },
    red: {
        width: '100%',
        height: 10,
        backgroundColor: '#F2666C',
        borderBottomLeftRadius: 5,
        borderBottomRightRadius: 5,
    },
    green: {
        width: '100%',
        height: 10,
        backgroundColor: '#169B53',
        borderBottomLeftRadius: 5,
        borderBottomRightRadius: 5,
    },
    header:{
        flexDirection: 'row',
        alignItems: 'center',
        width: '100%',
        paddingLeft: 5,
        paddingRight: 5,
        paddingTop: 5,
    },
    nombre:{
        fontSize: 18,
        marginLeft: 10,
    },
})

export default SolicitudesItem