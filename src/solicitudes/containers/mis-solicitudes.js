import React, { Component } from 'react'
import {
    View,
    Text,
    SafeAreaView,
    FlatList,
    RefreshControl,
    StyleSheet
} from 'react-native'

import { connect } from 'react-redux'
import { NavigationActions } from 'react-navigation'

import ActionButton from 'react-native-action-button'

import Header from '../../components/header'
import EmptySolicitudItem from '../components/empty-solicitud-item'
import SolicitudItem from '../components/solicitud-item'

import API from '../../../utils/api'
import showAlert from '../../../utils/alert'

class MisSolicitudes extends Component{
    state = {
        refreshing: false
    }

    keyExtractor = item => item.id.toString()
    renderEmptyComponent = () => <EmptySolicitudItem
                                    text="Aún no hay Solicitudes"
                                />

    renderItem = ({item}) => <SolicitudItem
                                {...item}
                                onPress={this.handleSingleEvent.bind(null, item.id)}
                            />
    renderLastItem = () => <View style={styles.emptyItem}></View>

    handleSingleEvent = id => {
        this.props.dispatch({
            type: 'SELECT_MI_SOLICITUD',
            payload: {
                id
            }
        })

        this.props.dispatch(NavigationActions.navigate({
            routeName: 'MiSolicitud'
        }))
    }

    handleOnBack = () => {
        this.props.dispatch(NavigationActions.back())
    }

    handleAddSolicitud = () => {
        this.props.dispatch(NavigationActions.navigate({
            routeName: 'AddSolicitud'
        }))
    }

    listMisSolicitudes = async () => {
        this.setState({
            refreshing: true
        })
        const responseMisSolicitudes = await API.getMisSolicitudes(this.props.user.id)
        console.log(responseMisSolicitudes)

        if(responseMisSolicitudes.success){
            this.props.dispatch({
                type: 'LOAD_MIS_SOLICITUDES_DATA',
                payload:{
                    solicitudes: responseMisSolicitudes.response
                }
            })
        }else{
            showAlert("Ocurrio un error al listar las solicitudes", 'error', 'long')
        }

        this.setState({
            refreshing: false
        })
    }

    handleRefresh = () => {
        this.listMisSolicitudes()
    }

    componentDidMount() {
        this.listMisSolicitudes()
    }

    render(){
        return(
            <SafeAreaView>
                <View
                    style={styles.containier}
                >
                    <Header
                        title="Mis Solicitudes"
                        back
                        onPress={this.handleOnBack}
                    />

                    {
                        /* this.props.misSolicitudes && */
                        false &&
                        <View>
                            <Text>FILTROS</Text>
                        </View>
                    }

                    <FlatList
                        style={styles.list}
                        keyExtractor={this.keyExtractor}
                        data={this.props.misSolicitudes}
                        ListEmptyComponent={this.renderEmptyComponent}
                        renderItem={this.renderItem}
                        ListFooterComponent={this.renderLastItem}
                        refreshControl={
                            <RefreshControl
                                refreshing={this.state.refreshing}
                                onRefresh={this.handleRefresh}
                            />
                        }
                    />
                    <ActionButton
                        style={styles.fab}
                        buttonColor="#3E2FD8"
                        onPress={this.handleAddSolicitud}
                    />
                </View>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    containier:{
        height: '100%'
    },
    list:{
        height: '100%'
    },
    emptyItem:{
        height: 0,
        marginBottom: 150
    },
    fab:{
        display: 'flex',
        marginTop: '90%'
    },
})

function mapStateToProps(state, props){
    return{
        misSolicitudes: state.solicitudes.misSolicitudesList,
        user: state.user
    }
}

export default connect(mapStateToProps)(MisSolicitudes)