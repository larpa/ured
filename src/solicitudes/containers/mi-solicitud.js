import React, { Component } from 'react'
import {
    View,
    Text,
    Image,
    Modal,
    SafeAreaView,
    TouchableWithoutFeedback,
    ScrollView,
    StyleSheet
} from 'react-native'

import { connect } from 'react-redux'

import API from '../../../utils/api'
import showAlert from '../../../utils/alert'

import moment from 'moment'
import ImageZoomViewer from 'react-native-image-zoom-viewer'

class Solicitud extends Component{

    state = {
        fotos: [],
        observaciones: [],
        viewerVisible: false,
        viewerIndex: 0
    }

    handleShowImageViewer = (index) => {
        if(index !== -1){
            this.setState({
                viewerIndex: index
            })
        }
        this.setState({
            viewerVisible: !this.state.viewerVisible
        })
    }

    componentDidMount = async () => {
        if(this.props.solicitud.adjunto == 'S'){
            const responseAdjuntos = await API.getAdjuntos(this.props.solicitud.id)
            if(responseAdjuntos.success){
                this.setState({
                    fotos: responseAdjuntos.response
                })
            }else{
                showAlert('Ocurrio un problema al cargar las fotos adjuntas', 'error', 'long')
            }
        }
        // revisar si hay observaciones e esta solicitud y mostrarlas
        const responseObservaciones = await API.getObservaciones(this.props.solicitud.id)
        if(responseObservaciones.success){
            this.setState({
                observaciones: responseObservaciones.response
            })
        }
    }

    render(){
        return(
            <SafeAreaView>
                <ScrollView
                    style={styles.fullContainer}
                >
                    {
                        this.props.solicitud.estado === 'G' &&
                            <View style={styles.black}></View>
                    }
                    {
                        this.props.solicitud.estado === 'E' &&
                            <View style={styles.black}></View>
                    }
                    {
                        this.props.solicitud.estado === 'D' &&
                            <View style={styles.yellow}></View>
                    }
                    {
                        this.props.solicitud.estado === 'P' &&
                            <View style={styles.green}></View>
                    }
                    {
                        this.props.solicitud.estado === 'R' &&
                            <View style={styles.red}></View>
                    }
                    <View
                        style={styles.container}
                    >
                        <View
                            style={styles.doubleView}
                        >
                            <Text
                                style={styles.topLabel}
                            >
                                {this.props.solicitud.id}
                            </Text>
                            <Text
                                style={styles.topLabel}
                            >
                                {moment(this.props.solicitud.timestamp_registro).format("DD/MM/YYYY")}
                            </Text>
                        </View>
                        <Text
                            style={styles.titleLabel}
                        >
                            {this.props.solicitud.tipo_solicitud}
                        </Text>
                        <View
                            style={styles.doubleView}
                        >
                            <Text
                                style={styles.bottomLabel}
                            >
                                {this.props.solicitud.urgente == 'U' ? "URGENTE" : "NORMAL"}
                            </Text>
                            <Text
                                style={styles.bottomLabel}
                            >
                                {this.props.solicitud.descripcion_estado.toUpperCase()}
                            </Text>
                        </View>
                        <Text
                            style={styles.textLabel}
                        >
                            Motivo:
                        </Text>
                        <Text
                            style={styles.motivoContent}
                        >
                            {this.props.solicitud.motivo}
                        </Text>
                        {
                            this.props.solicitud.adjunto == 'S' &&
                            <View>
                                <Text
                                    style={styles.textLabel}
                                >
                                    Adjuntos:
                                </Text>
                                <ScrollView
                                    horizontal={true}
                                    style={styles.mediaBox}
                                >
                                    {
                                        this.state.fotos.map((item, i) => {
                                            return(
                                                <TouchableWithoutFeedback
                                                    key={i}
                                                    onPress={this.handleShowImageViewer.bind(null, i)}
                                                >
                                                    <Image
                                                        source={{uri: item.url}}
                                                        style={styles.image}
                                                    />
                                                </TouchableWithoutFeedback>
                                            )
                                        })
                                    }
                                </ScrollView>
                            </View>
                        }
                        <Text
                            style={styles.textLabel}
                        >
                            Fecha desde:
                        </Text>
                        {
                            this.props.solicitud.hora_desde != '00:00:00' ?
                                <View
                                    style={styles.doubleView}
                                >
                                    <Text
                                        style={styles.dateLabel}
                                    >
                                        {moment(this.props.solicitud.fecha_desde).format('DD/MM/YYYY')}
                                    </Text>

                                    <Text
                                        style={styles.dateLabel}
                                    >
                                        {this.props.solicitud.hora_desde.split(":")[0]}:{this.props.solicitud.hora_desde.split(":")[1]}
                                    </Text>
                                </View>
                            :
                                <Text
                                    style={styles.dateLabel}
                                >
                                    {moment(this.props.solicitud.fecha_desde).format('DD/MM/YYYY')}
                                </Text>
                        }
                        <Text
                            style={styles.textLabel}
                        >
                            Fecha hasta:
                        </Text>
                        {
                            this.props.solicitud.hora_hasta != '00:00:00' ?
                                <View
                                    style={styles.doubleView}
                                >
                                    <Text
                                        style={styles.dateLabel}
                                    >
                                        {moment(this.props.solicitud.fecha_hasta).format('DD/MM/YYYY')}
                                    </Text>

                                    <Text
                                        style={styles.dateLabel}
                                    >
                                        {this.props.solicitud.hora_hasta.split(":")[0]}:{this.props.solicitud.hora_hasta.split(":")[1]}
                                    </Text>
                                </View>
                            :
                                <Text
                                    style={styles.dateLabel}
                                >
                                    {moment(this.props.solicitud.fecha_hasta).format('DD/MM/YYYY')}
                                </Text>
                        }
                        {
                            this.state.observaciones.map((item, i) => {
                                return(
                                    <View
                                        key={i}
                                    >
                                        <Text
                                            style={styles.observacionTextLabel}
                                        >
                                            Observación de {item.personal}
                                        </Text>
                                        <Text
                                            style={styles.motivoContent}
                                        >
                                            {item.observacion}
                                        </Text>
                                    </View>
                                )
                            })
                        }
                    </View>
                    <Modal
                        visible={this.state.viewerVisible}
                        transparent={true}
                    >
                        <ImageZoomViewer
                            imageUrls={this.state.fotos}
                            index={this.state.viewerIndex}
                            onClick={this.handleShowImageViewer.bind(null, -1)}
                        />
                    </Modal>
                </ScrollView>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    fullContainer:{
        height: '100%',
    },
    container:{
        marginLeft: 15,
        marginRight: 15,
    },
    doubleView:{
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginTop: 15,
    },
    topLabel:{
        fontSize: 20,
        fontWeight: '300',
        color: '#757779'
    },
    titleLabel: {
        fontSize: 22,
        marginTop: 10,
    },
    textLabel:{
        fontSize: 18,
        fontWeight: '400',
        marginTop: 15,
    },
    observacionTextLabel:{
        fontSize: 18,
        fontWeight: '400',
        marginTop: 35,
    },
    motivoContent: {
        fontSize: 18,
        fontWeight: '400',
        color: '#757779',
        marginLeft: 15,
        marginTop: 10,
    },
    bottomLabel:{
        fontSize: 12,
        fontWeight: 'bold',
    },
    black: {
        width: '95%',
        height: 35,
        backgroundColor: '#1C1C1C',
        borderRadius: 5,
        margin: 10,
    },
    yellow: {
        width: '95%',
        height: 35,
        backgroundColor: '#DFFF13',
        borderRadius: 5,
        margin: 10,
    },
    red: {
        width: '95%',
        height: 35,
        backgroundColor: '#F2666C',
        borderRadius: 5,
        margin: 10,
    },
    green: {
        width: '95%',
        height: 35,
        backgroundColor: '#169B53',
        borderRadius: 5,
        margin: 10,
    },
    mediaBox:{
        flexDirection: 'row',
        marginTop: 10,
    },
    image: {
        height: 200,
        width: 100,
        marginLeft: 10,
        backgroundColor: '#4B4B4B',
    },
    dateLabel:{
        fontSize: 22,
        fontWeight: '300',
        marginTop: 5,
    },
})

function mapStateToProps(state, props){
    return{
        user: state.user,
        solicitud: state.solicitudes.miSolicitudSelected
    }
}

export default connect(mapStateToProps)(Solicitud)