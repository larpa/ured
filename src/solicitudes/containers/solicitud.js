import React, { Component } from 'react'
import {
    View,
    Text,
    Image,
    Modal,
    SafeAreaView,
    TouchableWithoutFeedback,
    KeyboardAvoidingView,
    ScrollView,
    StyleSheet
} from 'react-native'

import { connect } from 'react-redux'
import { NavigationActions } from 'react-navigation'

import ImageZoomViewer from 'react-native-image-zoom-viewer'
import Avatar from '../../components/avatar'
import Button from '../../components/button'
import TextInput from '../../components/text-input'

import API from '../../../utils/api'
import showAlert from '../../../utils/alert'

import moment from 'moment'

class Solicitud extends Component{

    state = {
        fotos: [],
        viewerVisible: false,
        viewerIndex: 0,
        observacion: '',
        rechazarButtonLoading: false,
        aceptButtonLoading: false,
    }

    handleShowImageViewer = (index) => {
        if(index !== -1){
            this.setState({
                viewerIndex: index
            })
        }
        this.setState({
            viewerVisible: !this.state.viewerVisible
        })
    }

    handleOnChangeObservacion = (value) => {
        this.setState({
            observacion: value
        })
    }

    handleRechazoButton = async () => {
        this.setState({
            rechazarButtonLoading: true
        })

        let observacion = this.state.observacion === '' ? "-" : this.state.observacion

        let fecha_desde = moment(this.props.solicitud.fecha_desde).format("YYYY-MM-DD")
        let fecha_hasta = moment(this.props.solicitud.fecha_hasta).format("YYYY-MM-DD")

        const response = await API.changeEstadoSolicitud(this.props.solicitud.id, observacion, fecha_desde, fecha_hasta, this.props.solicitud.hora_desde, this.props.solicitud.hora_hasta, 'R', this.props.user.id)

        if(response.success){
            showAlert("Rechazaste una solicitud", 'success', 'long')
            this.props.dispatch(NavigationActions.back())
        }else{
            showAlert("Ocurrio un error rechazando la solicitud", 'error', 'long')
        }
        this.setState({
            rechazarButtonLoading: false
        })
    }

    handleAceptButton = async () => {
        this.setState({
            aceptButtonLoading: true
        })

        let observacion = this.state.observacion === '' ? "-" : this.state.observacion

        let fecha_desde = moment(this.props.solicitud.fecha_desde).format("YYYY-MM-DD")
        let fecha_hasta = moment(this.props.solicitud.fecha_hasta).format("YYYY-MM-DD")

        let estado = 'D'
        if(this.props.user.area.toLowerCase().includes('personal')){
            console.log("$ Soy de gestion depersonal")
            estado = 'P'
        }else{
            console.log("# Soy de " + this.props.user.area )
        }

        const response = await API.changeEstadoSolicitud(this.props.solicitud.id, observacion, fecha_desde, fecha_hasta, this.props.solicitud.hora_desde, this.props.solicitud.hora_hasta, estado, this.props.user.id)

        if(response.success){
            showAlert("Aceptaste una solicitud", 'success', 'long')
            this.props.dispatch(NavigationActions.back())
        }else{
            showAlert("Ocurrio un error rechazando la solicitud", 'error', 'long')
        }
        this.setState({
            aceptButtonLoading: false
        })
    }

    componentDidMount = async () => {
        if(this.props.solicitud.adjunto == 'S'){
            const responseAdjuntos = await API.getAdjuntos(this.props.solicitud.id)
            if(responseAdjuntos.success){
                this.setState({
                    fotos: responseAdjuntos.response
                })
            }else{
                showAlert('Ocurrio un problema al cargar las fotos adjuntas', 'error', 'long')
            }
        }
    }

    render(){
        return(
            <SafeAreaView>
                <KeyboardAvoidingView
                    behavior="padding"
                >
                    <ScrollView
                        style={styles.fullContainer}
                    >
                        {
                            this.props.solicitud.estado === 'G' &&
                                <View style={styles.black}></View>
                        }
                        {
                            this.props.solicitud.estado === 'E' &&
                                <View style={styles.black}></View>
                        }
                        {
                            this.props.solicitud.estado === 'D' &&
                                <View style={styles.yellow}></View>
                        }
                        {
                            this.props.solicitud.estado === 'P' &&
                                <View style={styles.green}></View>
                        }
                        {
                            this.props.solicitud.estado === 'R' &&
                                <View style={styles.red}></View>
                        }
                        <View
                            style={styles.container}
                        >
                            <View
                                style={styles.doubleView}
                            >
                                <Text
                                    style={styles.topLabel}
                                >
                                    {this.props.solicitud.id}
                                </Text>
                                <Text
                                    style={styles.topLabel}
                                >
                                    {moment(this.props.solicitud.timestamp_registro).format("DD/MM/YYYY")}
                                </Text>
                            </View>
                            <View
                                style={styles.header}
                            >
                                <Avatar 
                                    size="medium"
                                    rounded
                                    title={this.props.solicitud.solicitante.slice(0,2)}
                                    source={{uri: this.props.solicitud.foto_solicitante}}
                                />
                                <View>
                                    <Text
                                        style={styles.nombre}
                                    >
                                        {this.props.solicitud.solicitante}
                                    </Text>
                                </View>
                            </View>
                            <Text
                                style={styles.titleLabel}
                            >
                                {this.props.solicitud.tipo_solicitud}
                            </Text>
                            <View
                                style={styles.doubleView}
                            >
                                <Text
                                    style={styles.bottomLabel}
                                >
                                    {this.props.solicitud.urgente == 'U' ? "URGENTE" : "NORMAL"}
                                </Text>
                                <Text
                                    style={styles.bottomLabel}
                                >
                                    {this.props.solicitud.descripcion_estado.toUpperCase()}
                                </Text>
                            </View>
                            <Text
                                style={styles.textLabel}
                            >
                                Motivo:
                            </Text>
                            <Text
                                style={styles.motivoContent}
                            >
                                {this.props.solicitud.motivo}
                            </Text>
                            {
                                this.props.solicitud.adjunto == 'S' &&
                                <View>
                                    <Text
                                        style={styles.textLabel}
                                    >
                                        Adjuntos:
                                    </Text>
                                    <ScrollView
                                        horizontal={true}
                                        style={styles.mediaBox}
                                    >
                                        {
                                            this.state.fotos.map((item, i) => {
                                                return(
                                                    <TouchableWithoutFeedback
                                                        key={i}
                                                        onPress={this.handleShowImageViewer.bind(null, i)}
                                                    >
                                                        <Image
                                                            source={{uri: item.url}}
                                                            style={styles.image}
                                                        />
                                                    </TouchableWithoutFeedback>
                                                )
                                            })
                                        }
                                    </ScrollView>
                                </View>
                            }
                            <Text
                                style={styles.textLabel}
                            >
                                Fecha desde:
                            </Text>
                            {
                                this.props.solicitud.hora_desde != '00:00:00' ?
                                    <View
                                        style={styles.doubleView}
                                    >
                                        <Text
                                            style={styles.dateLabel}
                                        >
                                            {moment(this.props.solicitud.fecha_desde).format('DD/MM/YYYY')}
                                        </Text>

                                        <Text
                                            style={styles.dateLabel}
                                        >
                                            {this.props.solicitud.hora_desde.split(":")[0]}:{this.props.solicitud.hora_desde.split(":")[1]}
                                        </Text>
                                    </View>
                                :
                                    <Text
                                        style={styles.dateLabel}
                                    >
                                        {moment(this.props.solicitud.fecha_desde).format('DD/MM/YYYY')}
                                    </Text>
                            }
                            <Text
                                style={styles.textLabel}
                            >
                                Fecha hasta:
                            </Text>
                            {
                                this.props.solicitud.hora_hasta != '00:00:00' ?
                                    <View
                                        style={styles.doubleView}
                                    >
                                        <Text
                                            style={styles.dateLabel}
                                        >
                                            {moment(this.props.solicitud.fecha_hasta).format('DD/MM/YYYY')}
                                        </Text>

                                        <Text
                                            style={styles.dateLabel}
                                        >
                                            {this.props.solicitud.hora_hasta.split(":")[0]}:{this.props.solicitud.hora_hasta.split(":")[1]}
                                        </Text>
                                    </View>
                                :
                                    <Text
                                        style={styles.dateLabel}
                                    >
                                        {moment(this.props.solicitud.fecha_hasta).format('DD/MM/YYYY')}
                                    </Text>
                            }
                            <View
                                style={styles.inputBox}
                            >
                                <Text
                                    style={styles.labelText}
                                >
                                    Observación
                                </Text>
                                <TextInput
                                    style={styles.dinamicInput}
                                    onChangeText={this.handleOnChangeObservacion}
                                    value={this.state.observacion}
                                    returnKeyType="next"
                                    multiline={true}
                                    numberOfLines={3}
                                />
                            </View>
                            <View
                                style={styles.doubleView}
                            >
                                <Button
                                    component
                                    onPress={this.handleRechazoButton}
                                    loading={this.state.rechazarButtonLoading}
                                >
                                    <View
                                        style={styles.rechazarButtonBox}
                                    >
                                        <Text
                                            style={styles.buttonText}
                                        >
                                            Rechazar
                                        </Text>
                                    </View>
                                </Button>
                                <Button
                                    component
                                    onPress={this.handleAceptButton}
                                    loading={this.state.aceptButtonLoading}
                                >
                                    <View
                                        style={styles.aceptButtonBox}
                                    >
                                        <Text
                                            style={styles.buttonText}
                                        >
                                            Aceptar
                                        </Text>
                                    </View>
                                </Button>
                            </View>

                        </View>
                        <Modal
                            visible={this.state.viewerVisible}
                            transparent={true}
                        >
                            <ImageZoomViewer
                                imageUrls={this.state.fotos}
                                index={this.state.viewerIndex}
                                onClick={this.handleShowImageViewer.bind(null, -1)}
                            />
                        </Modal>
                    </ScrollView>
                </KeyboardAvoidingView>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    fullContainer:{
        height: '100%',
    },
    container:{
        marginLeft: 15,
        marginRight: 15,
    },
    doubleView:{
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginTop: 15,
    },
    topLabel:{
        fontSize: 20,
        fontWeight: '300',
        color: '#757779'
    },
    titleLabel: {
        fontSize: 22,
        marginTop: 10,
    },
    textLabel:{
        fontSize: 18,
        fontWeight: '400',
        marginTop: 15,
    },
    motivoContent: {
        fontSize: 18,
        fontWeight: '400',
        color: '#757779',
        marginLeft: 15,
        marginTop: 10,
    },
    bottomLabel:{
        fontSize: 12,
        fontWeight: 'bold',
    },
    black: {
        width: '95%',
        height: 35,
        backgroundColor: '#1C1C1C',
        borderRadius: 5,
        margin: 10,
    },
    yellow: {
        width: '95%',
        height: 35,
        backgroundColor: '#DFFF13',
        borderRadius: 5,
        margin: 10,
    },
    red: {
        width: '95%',
        height: 35,
        backgroundColor: '#F2666C',
        borderRadius: 5,
        margin: 10,
    },
    green: {
        width: '95%',
        height: 35,
        backgroundColor: '#169B53',
        borderRadius: 5,
        margin: 10,
    },
    mediaBox:{
        flexDirection: 'row',
        marginTop: 10,
    },
    image: {
        height: 200,
        width: 100,
        marginLeft: 10,
        backgroundColor: '#4B4B4B',
    },
    dateLabel:{
        fontSize: 22,
        fontWeight: '300',
        marginTop: 5,
    },
    header:{
        flexDirection: 'row',
        alignItems: 'center',
        width: '100%',
        paddingLeft: 5,
        paddingRight: 5,
        paddingTop: 15,
        paddingBottom: 5,
    },
    nombre:{
        fontSize: 18,
        marginLeft: 10,
    },
    rechazarButtonBox:{
        width: 150,
        marginTop: 20,
        marginLeft: '5%',
        marginBottom: 40,
		padding: 15,
		alignItems: 'center',
		borderRadius: 10,
		backgroundColor: '#FF2C2D',
    },
    aceptButtonBox:{
        width: 150,
        marginTop: 20,
        marginLeft: '5%',
        marginBottom: 40,
		padding: 15,
		alignItems: 'center',
		borderRadius: 10,
		backgroundColor: '#4A3DDD',
    },
    buttonText:{
		fontSize: 18,
		color: 'white',
		fontWeight: '600',
    },
    inputBox:{
        marginLeft: 15,
        marginRight: 15,
        marginTop: 20,
    },
    labelText:{
        fontSize: 18,
        color: '#4B4B4B',
    },
    dinamicInput:{
        marginTop: 10,
        marginRight: 15,
    },
})

function mapStateToProps(state, props){
    return{
        user: state.user,
        solicitud: state.solicitudes.solicitudSelected
    }
}

export default connect(mapStateToProps)(Solicitud)