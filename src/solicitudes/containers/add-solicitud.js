import React, { Component } from 'react'
import {
    View,
    Text,
    SafeAreaView,
    ScrollView,
    Switch,
    KeyboardAvoidingView,
    StyleSheet
} from 'react-native'

import { connect } from 'react-redux'
import { NavigationActions } from 'react-navigation'

import Header from '../../components/header'
import TextInput from '../../components/text-input'
import ImagePicker from '../../../utils/image-picker'
import ImageAttachBox from '../../components/image-attach-box'
import Button from '../../components/button'
import showAlert from '../../../utils/alert'

import PickerSelect from 'react-native-picker-select'
import DatePicker from 'react-native-datepicker'
import moment from 'moment'
import API from '../../../utils/api'

class AddSolicitud extends Component{

    state = {
        switchUrgente: false,
        registerButtonLoading: false,
        motivo: '',
        fechaDesde: moment().format("YYYY-MM-DD"),
        fechaHasta: moment().add(1, 'day').format("YYYY-MM-DD"),
        horaDesde: moment().format("LT"),
        horaHasta: moment().add(1, 'hour').format("LT"),
        files: [],
        tipoSolicitudList: [],
        tipoSelected: 0,
        numOfDifference: '1 día',
    }

    handleOnBack = () => {
        this.props.dispatch(NavigationActions.back())
    }

    handleSwitchUrgente = () => {
        this.setState({
            switchUrgente: !this.state.switchUrgente
        })
    }

    handleOnChangeMotivo = (value) => {
        this.setState({
            motivo: value
        })
    }

    handleOnChangeFechaDesde = (value) => {
        this.setState({
            fechaDesde: value
        })
        this.updateDiffDates(value, this.state.fechaHasta)
    }

    handleOnChangeHoraDesde = (value) => {
        this.setState({
            horaDesde: value
        })
        this.updateDiffTimes(value, this.state.horaHasta)
    }

    handleOnChangeHoraHasta = (value) => {
        this.setState({
            horaHasta: value
        })
        this.updateDiffTimes(this.state.horaDesde, value)
    }

    handleOnChangeFechaHasta = (value) => {
        this.setState({
            fechaHasta: value
        })
        this.updateDiffDates(this.state.fechaDesde, value)
    }

    updateDiffDates = (desde, hasta) => {
        let a = moment(desde)
        let b = moment(hasta)
        let dif = b.diff(a, 'days')
        let d = dif > 1 ? `${dif} días` : `${dif} día`
        this.setState({
            numOfDifference: d
        })
    }

    updateDiffTimes = (desde, hasta) => {
        let a = moment(desde, 'LT')
        let b = moment(hasta, 'LT')
        let dif = b.diff(a, 'hours')
        let d = dif > 1 ? `${dif} horas` : `${dif} hora`
        this.setState({
            numOfDifference: d
        })
    }

    handleAddFile = async () => {
        const file = await ImagePicker()
        if(!file.didCancel){
            let bk_files = this.state.files
            bk_files.push(file.uri)
            this.setState({
                files: bk_files
            })
        }
    }

    handleRegisterButton = async () => {

        if(this.state.tipoSelected === 0){
            showAlert("Debes escoger un tipo de solicitud", 'warning', 'long')
            return
        }

        this.setState({
            registerButtonLoading: true
        })

        let urgente = this.state.switchUrgente ? 'U' : 'N'

        let motivo = this.state.motivo === '' ? '-' : this.state.motivo

        let fecha_desde = this.state.fechaDesde
        let fecha_hasta = this.state.fechaHasta

        let hora_desde = '00:00:00'
        let hora_hasta = '00:00:00'

        if(this.state.tipoSelected == 4){
            hora_desde = moment(this.state.horaDesde, 'LT').format("HH:mm:ss")
            hora_hasta = moment(this.state.horaHasta, 'LT').format("HH:mm:ss")
        }

        const response = await API.addSolicitud(this.props.user.id, this.state.tipoSelected, urgente, fecha_desde, fecha_hasta, hora_desde, hora_hasta, motivo, this.state.files)

        this.setState({
            registerButtonLoading: false
        })

        if(response.success){
            showAlert("Solicitud registrada", "success", 'long')
            this.props.dispatch(NavigationActions.back())
        }else{
            showAlert("Ocurrio un error al registrar tu solicitud.", "error", 'long')
        }
    }

    loadTipoSolicitudes = async () => {
        const responseTipoSolicitudes = await API.getTipoSolicitud()
        console.log("request response: ", responseTipoSolicitudes)
        let holder = [];
        if(responseTipoSolicitudes.success){
            responseTipoSolicitudes.response.map(value => {
                console.log("-> ", value)
                holder.push({label: value.descripcion, value: value.id})
            })
        }else{
            showAlert('Ocurrio un error al cargar los tipos de solicitudes.', 'error', 'long')
        }
        this.setState({
            tipoSolicitudList: holder
        })
    }

    componentDidMount = async () => {
        this.loadTipoSolicitudes()
    }

    render(){
        return(
            <SafeAreaView>
                <KeyboardAvoidingView
                    behavior="padding"
                >
                    <ScrollView
                        style={styles.container}
                    >
                        <Header
                            title="Nueva solicitud"
                            back
                            onPress={this.handleOnBack}
                        />
                        <View
                            style={styles.inputBox}
                        >
                            <Text
                                style={styles.labelText}
                            >
                                Prioridad
                            </Text>
                            <View
                                style={styles.doubleInputBox}
                            >
                                <Text
                                    style={styles.specialLabel}
                                >
                                    Urgente
                                </Text>
                                <Switch
                                    style={styles.switchUrgente}
                                    onValueChange={this.handleSwitchUrgente}
                                    value={this.state.switchUrgente}
                                />
                            </View>
                        </View>
                        <View
                            style={styles.inputBox}
                        >
                            <Text
                                style={styles.labelText}
                            >
                                Tipo de solicitud
                            </Text>
                            <PickerSelect
                                hideIcon={true}
                                placeholder={{
                                    label: 'Selecciona un tipo',
                                    value: 0,
                                    key: 0,
                                }}
                                items={this.state.tipoSolicitudList}
                                onValueChange={(value) => {
                                    this.setState({
                                        tipoSelected: value
                                    })
                                }}
                                style={{...specialStylesForPicket}}
                                value={this.state.tipoSelected}
                            />
                        </View>
                        {
                            this.state.tipoSelected == 4 ?
                                <View
                                    style={styles.inputBox}
                                >
                                    <Text
                                        style={styles.labelText}
                                    >
                                        Fecha desde
                                    </Text>
                                    <View
                                        style={styles.doubleInputBox}
                                    >
                                        <DatePicker
                                            style={styles.doubleDateInputText}
                                            customStyles={dateInputStyles}
                                            date={this.state.fechaDesde}
                                            mode="date"
                                            locale={'es'}
                                            placeholder="Fecha desde"
                                            format="YYYY-MM-DD"
                                            minDate={moment().format("YYYY-MM-DD")}
                                            confirmBtnText="Ok"
                                            cancelBtnText="Cancelar"
                                            showIcon={false}
                                            onDateChange={this.handleOnChangeFechaDesde}
                                        />
                                        <DatePicker
                                            style={styles.doubleDateInputText}
                                            customStyles={dateInputStyles}
                                            date={this.state.horaDesde}
                                            mode="time"
                                            locale={'es'}
                                            placeholder="Hora desde"
                                            format="LT"
                                            confirmBtnText="Ok"
                                            cancelBtnText="Cancelar"
                                            showIcon={false}
                                            onDateChange={this.handleOnChangeHoraDesde}
                                        />
                                    </View>
                                </View>
                            :
                                <View
                                    style={styles.inputBox}
                                >
                                    <Text
                                        style={styles.labelText}
                                    >
                                        Fecha desde
                                    </Text>
                                    <DatePicker
                                        style={styles.dateInputText}
                                        customStyles={dateInputStyles}
                                        date={this.state.fechaDesde}
                                        mode="date"
                                        locale={'es'}
                                        placeholder="Fecha desde"
                                        format="YYYY-MM-DD"
                                        minDate={moment().format("YYYY-MM-DD")}
                                        confirmBtnText="Ok"
                                        cancelBtnText="Cancelar"
                                        showIcon={false}
                                        onDateChange={this.handleOnChangeFechaDesde}
                                    />
                                </View>
                        }
                        {
                            this.state.tipoSelected == 4 ?
                                <View
                                        style={styles.inputBox}
                                    >
                                        <Text
                                            style={styles.labelText}
                                        >
                                            Fecha hasta
                                        </Text>
                                        <View
                                            style={styles.doubleInputBox}
                                        >
                                            <DatePicker
                                                style={styles.doubleDateInputText}
                                                customStyles={dateInputStyles}
                                                date={this.state.fechaHasta}
                                                mode="date"
                                                locale={'es'}
                                                placeholder="Fecha hasta"
                                                format="YYYY-MM-DD"
                                                minDate={moment().format("YYYY-MM-DD")}
                                                confirmBtnText="Ok"
                                                cancelBtnText="Cancelar"
                                                showIcon={false}
                                                onDateChange={this.handleOnChangeFechaHasta}
                                            />
                                            <DatePicker
                                                style={styles.doubleDateInputText}
                                                customStyles={dateInputStyles}
                                                date={this.state.horaHasta}
                                                mode="time"
                                                locale={'es'}
                                                placeholder="Hora hasta"
                                                format="LT"
                                                minDate={moment().format("LT")}
                                                confirmBtnText="Ok"
                                                cancelBtnText="Cancelar"
                                                showIcon={false}
                                                onDateChange={this.handleOnChangeHoraHasta}
                                            />
                                        </View>
                                    </View>
                                :
                                    <View
                                        style={styles.inputBox}
                                    >
                                        <Text
                                            style={styles.labelText}
                                        >
                                            Fecha hasta
                                        </Text>
                                        <DatePicker
                                            style={styles.dateInputText}
                                            customStyles={dateInputStyles}
                                            date={this.state.fechaHasta}
                                            mode="date"
                                            locale={'es'}
                                            placeholder="Fecha hasta"
                                            format="YYYY-MM-DD"
                                            minDate={moment().format("YYYY-MM-DD")}
                                            confirmBtnText="Ok"
                                            cancelBtnText="Cancelar"
                                            showIcon={false}
                                            onDateChange={this.handleOnChangeFechaHasta}
                                        />
                                    </View>
                        }
                        <View
                            style={styles.inputBox}
                        >
                            <Text
                                style={styles.labelText}
                            >
                                Tiempo solicitado
                            </Text>
                            <View
                                style={styles.numOfDaysBox}
                            >
                                <Text
                                    style={styles.numOfDaysText}
                                >
                                    {this.state.numOfDifference}
                                </Text>
                            </View>
                        </View>
                        <View
                            style={styles.inputBox}
                        >
                            <Text
                                style={styles.labelText}
                            >
                                Motivo
                            </Text>
                            <TextInput
                                style={styles.dinamicInput}
                                onChangeText={this.handleOnChangeMotivo}
                                value={this.state.motivo}
                                returnKeyType="next"
                                multiline={true}
                                numberOfLines={3}
                            />
                        </View>
                        <View
                            style={styles.inputBox}
                        >
                            <Text
                                style={styles.labelText}
                            >
                                Adjunto
                            </Text>
                            <View
                                style={styles.inputAttachmentBox}
                            >
                                {
                                    this.state.files.length === 0 ?
                                        <ImageAttachBox
                                            onPress={this.handleAddFile}
                                            addFile={true}
                                        />
                                    :
                                        <View></View>
                                }
                                {
                                    this.state.files.map((value, i) => {
                                        return(
                                            <ImageAttachBox
                                                key={i}
                                                onPress={()=>{
                                                    let newFileObject = this.state.files;
                                                    newFileObject.splice(i, 1);
                                                    this.setState({
                                                        files: newFileObject
                                                    })
                                                }}
                                                addFile={false}
                                                source={value}
                                            />
                                        )
                                    })
                                }
                                {
                                    this.state.files && this.state.files.length < 3 && this.state.files.length > 0 ?
                                        <ImageAttachBox
                                            onPress={this.handleAddFile}
                                            addFile={true}
                                        />
                                    :
                                        <View></View>
                                }
                            </View>
                        </View>
                        <Button
                            component
                            color='#4A3DDD'
                            onPress={this.handleRegisterButton}
                            loading={this.state.registerButtonLoading}
                        >
                            <View
                                style={styles.buttonBox}
                            >
                                <Text
                                    style={styles.buttonText}
                                >
                                    Registrar
                                </Text>
                            </View>
                        </Button>
                    </ScrollView>
                </KeyboardAvoidingView>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        height: '100%',
        paddingBottom: 20,
    },
    inputBox:{
        marginLeft: 15,
        marginRight: 15,
        marginTop: 20,
    },
    labelText:{
        fontSize: 18,
        color: '#4B4B4B',
    },
    doubleInputBox:{
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    specialLabel:{
        fontSize: 26,
        fontWeight: '300',
    },
    switchUrgente:{
        marginTop: 5,
    },
    dateInputText:{
        width: '93%',
    },
    doubleDateInputText:{
        width: '40%',
        marginLeft: 10,
    },
    dinamicInput:{
        marginTop: 10,
        marginRight: 15,
    },
    inputAttachmentBox: {
        flexDirection: 'row',
        marginTop: 10,
    },
    buttonBox:{
        width: '90%',
        marginTop: 20,
        marginLeft: '5%',
        marginBottom: 40,
		padding: 10,
		alignItems: 'center',
		borderRadius: 10,
		backgroundColor: '#4A3DDD',
    },
    buttonText:{
		fontSize: 18,
		color: 'white',
		fontWeight: '600',
    },
    numOfDaysBox:{
        alignItems: 'center',
        width: '100%',
    },
    numOfDaysText:{
        fontSize: 26,
        margin: 15,
        fontWeight: '400',
    }
})

const specialStylesForPicket = StyleSheet.create({
    inputIOS: {
        fontSize: 20,
        color: 'black',
        marginTop:5,
    },
    inputAndroid: {
        fontSize: 20,
        color: 'black',
        marginTop:5,
    },
})

const dateInputStyles = {
    dateInput:{
        borderLeftWidth: 0,
        borderRightWidth: 0,
        borderTopWidth: 0,
        borderBottomWidth: 0.8,
        borderBottomColor: '#999999',
        height: 30,
        alignItems: 'flex-start'
    },
    dateText:{
        fontSize: 18,
    },
}

function mapStateToProps(state, props){
    return{
        user: state.user
    }
}

export default connect(mapStateToProps)(AddSolicitud)