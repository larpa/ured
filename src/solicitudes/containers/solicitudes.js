import React, { Component } from 'react'
import {
    View,
    Text,
    SafeAreaView,
    FlatList,
    RefreshControl,
    StyleSheet
} from 'react-native'

import { connect } from 'react-redux'
import { NavigationActions } from 'react-navigation'

import Header from '../../components/header'
import EmptySolicitudItem from '../components/empty-solicitud-item'
import SolicitudesItem from '../components/solicitudes-item'

import API from '../../../utils/api'
import showAlert from '../../../utils/alert'

class Solicitudes extends Component{
    state = {
        refreshing: false
    }

    keyExtractor = item => item.id.toString()

    renderEmptyComponent = () => <EmptySolicitudItem
                                    text="Aún no hay Solicitudes"
                                />

    renderItem = ({item}) => <SolicitudesItem
                                {...item}
                                onPress={this.handleSingleEvent.bind(null, item.id)}
                            />

    renderLastItem = () => <View style={styles.emptyItem}></View>

    handleSingleEvent = id => {
        this.props.dispatch({
            type: 'SELECT_SOLICITUD',
            payload: {
                id
            }
        })

        this.props.dispatch(NavigationActions.navigate({
            routeName: 'Solicitud'
        }))
    }

    handleOnBack = () => {
        this.props.dispatch(NavigationActions.back())
    }

    listMisSolicitudes = async () => {
        this.setState({
            refreshing: true
        })
        let responseMisSolicitudes
        if(this.props.user.area.toLowerCase().includes('personal')){
            responseMisSolicitudes = await API.getSolicitudesPersonal()
        }else{
            responseMisSolicitudes = await API.getSolicitudes(this.props.user.id)
        }
        console.log(responseMisSolicitudes)

        if(responseMisSolicitudes.success){
            this.props.dispatch({
                type: 'LOAD_SOLICITUDES_DATA',
                payload:{
                    solicitudes: responseMisSolicitudes.response
                }
            })
        }else{
            showAlert("Ocurrio un error al listar las solicitudes", 'error', 'long')
        }

        this.setState({
            refreshing: false
        })
    }

    handleRefresh = () => {
        this.listMisSolicitudes()
    }

    componentDidMount() {
        this.listMisSolicitudes()
    }

    render(){
        return(
            <SafeAreaView>
                <View
                    style={styles.containier}
                >
                    <Header
                        title="Solicitudes"
                        back
                        onPress={this.handleOnBack}
                    />

                    {
                        /* this.props.misSolicitudes && */
                        false &&
                        <View>
                            <Text>FILTROS</Text>
                        </View>
                    }

                    <FlatList
                        style={styles.list}
                        keyExtractor={this.keyExtractor}
                        data={this.props.solicitudes}
                        ListEmptyComponent={this.renderEmptyComponent}
                        renderItem={this.renderItem}
                        ListFooterComponent={this.renderLastItem}
                        refreshControl={
                            <RefreshControl
                                refreshing={this.state.refreshing}
                                onRefresh={this.handleRefresh}
                            />
                        }
                    />
                </View>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    containier:{
        height: '100%'
    },
    list:{
        height: '100%'
    },
    emptyItem:{
        height: 0,
        marginBottom: 150
    },
    fab:{
        display: 'flex',
        marginTop: '90%'
    },
})

function mapStateToProps(state, props){
    return{
        solicitudes: state.solicitudes.solicitudesList,
        user: state.user
    }
}

export default connect(mapStateToProps)(Solicitudes)