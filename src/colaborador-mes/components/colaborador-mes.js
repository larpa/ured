import React from 'react'
import {
    View,
    Text,
    StyleSheet
} from 'react-native'

import moment from 'moment'
import 'moment/locale/es'

import Avatar from '../../components/avatar'

function ColaboradorMes(props){
    return(
        <View
            style={styles.container}
        >
            <Text
                style={styles.fecha}
            >
                {`${capitalize(moment(props.fecha).format('MMMM'))} ${moment(props.fecha).format('YYYY')}`}
            </Text>
            <View
                style={styles.mainContent}
            >
                <Avatar 
                    size="large"
                    rounded
                    title={props.colaborador.slice(0,2)}
                    source={{uri: props.foto_perfil}}
                />
                <Text
                    style={styles.nombre}
                >
                    {props.colaborador}
                </Text>
                <Text
                    style={styles.rango}
                >
                    {`${props.area} ${props.puesto}`}
                </Text>
            </View>
            {
                props.observacion !== '-' &&
                    <View>
                        <Text
                            style={styles.observacionLabel}
                        >
                            Observación:
                        </Text>
                        <Text
                            style={styles.observacion}
                        >
                            {props.observacion}
                        </Text>
                    </View>
            }
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#FFFFFF',
        borderRadius: 15,
        shadowOffset:{
            width: 0,
            height: 0,
        },
        shadowColor: 'black',
        shadowOpacity: 0.4,
        elevation: 1,
        marginTop: 25,
        marginLeft: 10,
        marginRight: 10,
        padding: 10,
    },
    fecha:{
        fontSize: 28,
        color: '#191919',
        fontWeight: '200',
    },
    mainContent:{
        width: '100%',
        alignItems: 'center',
        marginTop: 15,
    },
    nombre:{
        marginTop: 10,
        fontSize: 24,
        textAlign: 'center',
    },
    rango:{
        marginTop: 5,
        fontSize: 18,
        color: '#191919',
        textAlign: 'center',
    },
    observacionLabel:{
        fontSize: 16,
        fontWeight: '300',
        marginTop: 20,
    },
    observacion: {
        fontSize: 20,
        marginTop: 10,
        marginLeft: 10,
    }
})

function capitalize(word){
    return word.charAt(0).toUpperCase() + word.slice(1)
}

export default ColaboradorMes

// area: "Creador"
// colaborador: "Luis Antonio Arce Paredes"
// fecha: "2019-10-10T05:00:00.000Z"
// foto_perfil: "https://firebasestorage.googleapis.com/v0/b/ured-f30fc.appspot.com/o/fotos_perfil%2F74225259.png?alt=media&token=83624ebb-ed10-437a-a5d7-9032c268f78a"
// id: 1
// id_perfil: 1
// observacion: "Gran labor en sus actividades. Llego primero a la meta."
// puesto: "programador"
