import React, { Component } from 'react'
import {
    View,
    Text,
    SafeAreaView,
    FlatList,
    RefreshControl,
    StyleSheet
} from 'react-native'

import { connect } from 'react-redux'
import { NavigationActions } from 'react-navigation'

import ActionButton from 'react-native-action-button'

import Header from '../../components/header'
import EmptyColaboradoresList from '../components/empty-colaboradores-list'
import ColaboradorMes from '../components/colaborador-mes'

import API from '../../../utils/api'
import showAlert from '../../../utils/alert'

class ColaboradorMesList extends Component{
    state = {
        refreshing: false
    }

    keyExtractor = item => item.id.toString()

    renderEmptyComponent = () => <EmptyColaboradoresList
                                    text="Aún no hay colaborador del mes."
                                />

    renderLastItem = () => <View style={styles.emptyItem}></View>

    renderItem = ({item}) => <ColaboradorMes
                                {...item}
                            />

    handleLoadList = async () => {
        this.setState({
            refreshing: true
        })

        const response = await API.getColaboradoresMes()

        console.log(response)

        if(response.success){
            this.props.dispatch({
                type: 'LOAD_COLABORADORES_LIST_DATA',
                payload: {
                    colaboradores: response.response
                }
            })
        }

        this.setState({
            refreshing: false
        })
    }

    handleOnBack = () => {
        this.props.dispatch(NavigationActions.back())
    }

    componentDidMount() {
        this.handleLoadList()
    }

    handleRefresh = () => {
        this.handleLoadList()
    }

    handleAddColaboradorMes = () => {
        console.log('agregando nuevo colaborador')
        this.props.dispatch(NavigationActions.navigate({
            routeName: 'AddColaboradorMes',
        }))
    }

    render(){
        return(
            <SafeAreaView>
                <View
                    style={styles.container}
                >
                    <Header
                        title="Colaborador del mes"
                        back
                        onPress={this.handleOnBack}
                    />
                    <FlatList
                        style={styles.list}
                        keyExtractor={this.keyExtractor}
                        data={this.props.colaboradoresList}
                        ListEmptyComponent={this.renderEmptyComponent}
                        renderItem={this.renderItem}
                        ListFooterComponent={this.renderLastItem}
                        refreshControl={
                            <RefreshControl
                                refreshing={this.state.refreshing}
                                onRefresh={this.handleRefresh}
                            />
                        }
                    />
                    {
                        true &&
                        <ActionButton
                            style={styles.fab}
                            buttonColor="#F36623"
                            onPress={this.handleAddColaboradorMes}
                        />
                    }
                </View>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        height: '100%',
    },
    list:{
        height: '100%'
    },
    emptyItem:{
        height: 0,
        marginBottom: 150
    },
})

function mapStateToProps(state, props){
    return{
        user: state.user,
        colaboradoresList: state.colaboradoresMes.colaboradoresList
    }
}

export default connect(mapStateToProps)(ColaboradorMesList)