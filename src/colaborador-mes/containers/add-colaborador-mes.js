import React, { Component } from 'react'
import {
    View,
    Text,
    SafeAreaView,
    StyleSheet
} from 'react-native'

import { connect } from 'react-redux'
import { NavigationActions } from 'react-navigation'

import Header from '../../components/header'
import Button from '../../components/button'
import TextInput from '../../components/text-input'

import PickerSelect from 'react-native-picker-select'

import API from '../../../utils/api'
import showAlert from '../../../utils/alert'

class AddColaboradorMes extends Component{
    state = {
        registerButtonLoading: false,
        observacion: '',
        mesSelected: '0',
        colaboradorSelected: 0,
        anoSelected: 2019,
        colaboradores: [],
        mes: [
            { label: 'Enero', value: '01', key: 1, },
            { label: 'Febrero', value: '02', key: 2, },
            { label: 'Marzo', value: '03', key: 3, },
            { label: 'Abril', value: '04', key: 4, },
            { label: 'Mayo', value: '05', key: 5, },
            { label: 'Junio', value: '06', key: 6, },
            { label: 'Julio', value: '07', key: 7, },
            { label: 'Agosto', value: '08', key: 8, },
            { label: 'Septiembre', value: '09', key: 9, },
            { label: 'Octubre', value: '10', key: 10, },
            { label: 'Noviembre', value: '11', key: 11, },
            { label: 'Diciembre', value: '12', key: 12, },
        ],
        anos: [
            { label: '2018', value: 2018, key: 1, },
            { label: '2019', value: 2019, key: 2, },
            { label: '2020', value: 2020, key: 3, },
            { label: '2021', value: 2021, key: 4, },
            { label: '2022', value: 2022, key: 5, },
            { label: '2023', value: 2023, key: 6, },
            { label: '2024', value: 2024, key: 7, },
            { label: '2025', value: 2025, key: 8, },
            { label: '2026', value: 2026, key: 9, },
            { label: '2027', value: 2027, key: 10, },
            { label: '2028', value: 2028, key: 11, },
        ]
    }

    handleOnBack = () => {
        this.props.dispatch(NavigationActions.back())
    }

    handleOnChangeObservacion = value => {
        this.setState({
            observacion: value
        })
    }

    componentDidMount = async () => {
        const response = await API.getColaboradores()
        console.log(response)
        if(response.success){
            let holder = [];
            response.response.map( (item, i) => {
                holder.push({label: item.colaborador, value: item.id, key: i})
            })
            this.setState({
                colaboradores: holder
            })
        }
    }

    handleRegisterButton = async () => {
        console.log('registrar')
        if(this.state.mesSelected === 0){
            showAlert('Debes seleccionar un mes', 'warning', 'long')
            return
        }
        if(this.state.anoSelected === 0){
            showAlert('Debes seleccionar un año', 'warning', 'long')
            return
        }
        if(this.state.colaboradorSelected === 0){
            showAlert('Debes seleccionar un colaborador', 'warning', 'long')
            return
        }
        let observacion = '-'
        if(this.state.observacion !== ''){
            observacion = this.state.observacion
        }

        this.setState({
            registerButtonLoading: true
        })

        let fecha = `${this.state.anoSelected}-${this.state.mesSelected}-01`

        const response = await API.addColaboradorMes(this.state.colaboradorSelected, fecha, observacion)

        if(response.success){
            showAlert('Colaborador agregado.', 'success', 'long')
            this.props.dispatch(NavigationActions.back())
        }else{
            showAlert('Ocurrio un problema al agregar el colaborador del mes, intentelo más tarde.', 'error', 'long')
        }

        this.setState({
            registerButtonLoading: false
        })

    }

    render(){
        return(
            <SafeAreaView>
                <Header
                    title="Agregar"
                    back
                    onPress={this.handleOnBack}
                />

                <View
                    style={styles.inputBox}
                >
                    <Text
                        style={styles.labelText}
                    >
                        Colaborador
                    </Text>
                    <PickerSelect
                        hideIcon={true}
                        placeholder={{
                            label: 'Selecciona un colaborador',
                            value: 0,
                            key: 0,
                        }}
                        items={this.state.colaboradores}
                        onValueChange={(value) => {
                            this.setState({
                                colaboradorSelected: value
                            })
                        }}
                        style={{...specialStylesForPicket}}
                        value={this.state.colaboradorSelected}
                    />
                </View>

                <View
                    style={styles.inputBox}
                >
                    <Text
                        style={styles.labelText}
                    >
                        Fecha
                    </Text>
                </View>
                <View
                    style={styles.inputDoubleBox}
                >
                    <View
                        style={styles.inputInnerDouble}
                    >
                        <Text
                            style={styles.labelText}
                        >
                            Mes
                        </Text>
                        <PickerSelect
                            hideIcon={true}
                            placeholder={{
                                label: 'Mes',
                                value: '0',
                                key: 0,
                            }}
                            items={this.state.mes}
                            onValueChange={(value) => {
                                this.setState({
                                    mesSelected: value
                                })
                            }}
                            style={{...specialStylesForPicket}}
                            value={this.state.mesSelected}
                        />
                    </View>
                    <View
                        style={styles.inputInnerDouble}
                    >
                        <Text
                            style={styles.labelText}
                        >
                            Año
                        </Text>
                        <PickerSelect
                            hideIcon={true}
                            placeholder={{
                                label: 'Año',
                                value: 0,
                                key: 0,
                            }}
                            items={this.state.anos}
                            onValueChange={(value) => {
                                this.setState({
                                    anoSelected: value
                                })
                            }}
                            style={{...specialStylesForPicket}}
                            value={this.state.anoSelected}
                        />
                    </View>
                </View>
                <View
                    style={styles.inputBox}
                >
                    <Text
                        style={styles.labelText}
                    >
                        Observacion
                    </Text>
                    <TextInput
                        style={styles.dinamicInput}
                        onChangeText={this.handleOnChangeObservacion}
                        value={this.state.observacion}
                        returnKeyType="next"
                        multiline={true}
                        numberOfLines={3}
                    />
                    <Button
                        component
                        color='#4A3DDD'
                        onPress={this.handleRegisterButton}
                        loading={this.state.registerButtonLoading}
                    >
                        <View
                            style={styles.buttonBox}
                        >
                            <Text
                                style={styles.buttonText}
                            >
                                Registrar
                            </Text>
                        </View>
                    </Button>
                </View>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    inputBox:{
        marginLeft: 15,
        marginRight: 15,
        marginTop: 20,
    },
    inputDoubleBox:{
        marginLeft: 15,
        marginRight: 15,
        marginTop: 20,
        flexDirection: 'row',
    },
    inputInnerDouble:{
        width: '45%'
    },
    labelText:{
        fontSize: 18,
        color: '#4B4B4B',
    },
    dinamicInput:{
        marginTop: 10,
        marginRight: 15,
    },
    buttonBox:{
        width: '90%',
        marginTop: 20,
        marginLeft: '5%',
        marginBottom: 40,
		padding: 10,
		alignItems: 'center',
		borderRadius: 10,
		backgroundColor: '#4A3DDD',
    },
    buttonText:{
		fontSize: 18,
		color: 'white',
		fontWeight: '600',
    },
})


const specialStylesForPicket = StyleSheet.create({
    inputIOS: {
        fontSize: 20,
        color: 'black',
        marginTop:5,
    },
    inputAndroid: {
        fontSize: 20,
        color: 'black',
        marginTop:5,
    },
})

function mapStateToProps(state, props){
    return{
        user: state.user
    }
}

export default connect(mapStateToProps)(AddColaboradorMes)