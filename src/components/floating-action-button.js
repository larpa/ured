import React from 'react'
import {
    TouchableOpacity,
    Text,
    StyleSheet,
} from 'react-native'

function floatingActionButton(props){
    return(
        <TouchableOpacity 
            onPress={props.onPress}
            style={styles.container}
        >
           
                <Text style={styles.text}>+</Text>
           
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    container:{
        height: 60,
        width: 60,
        borderRadius: 60 / 2,
        position: 'absolute',
        bottom: 60,
        right: 40,
        shadowOffset:{  width: 1,  height: 1,  },
        shadowColor: 'black',
        shadowOpacity: 0.8,
    },
    text:{
        color: 'white',
        fontSize: 45,
        fontWeight: '200',
    },
    button:{
        height: 60,
        width: 60,
        borderRadius: 60 / 2,
        alignItems: 'center',
    }
})

export default floatingActionButton