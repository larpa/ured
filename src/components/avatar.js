import React from 'react'
import {
    View,
    Image,
    Text,
    TouchableWithoutFeedback,
    StyleSheet
} from 'react-native'

let specialHeight = 40
let specialWidth = 40
let specialTextSize = 16

function Avatar(props){
    switch(props.size){
        case 'small':
            specialHeight = 40; specialWidth = 40; specialTextSize = 16;
            break
        case 'medium':
            specialHeight = 80; specialWidth = 80; specialTextSize = 16;
            break
        case 'large':
            specialHeight = 120; specialWidth = 120; specialTextSize = 24;
            break
        case 'xlarge':
            specialHeight = 160; specialWidth = 160; specialTextSize = 30;
            break
        default:
            specialHeight = 40; specialWidth = 40; specialTextSize = 40;
            break
    }
    if(!props.source && props.title){
        return (
            <TouchableWithoutFeedback
                onPress={props.onPress}
            >
                <View
                    style={{ height: specialHeight, width: specialWidth}}
                >
                    <Text
                        style={{fontSize: specialTextSize, color: '#FFFFFF'}}
                    >
                        {props.title}
                    </Text>
                </View>
            </TouchableWithoutFeedback>
        )
    }else{
        return (
            <TouchableWithoutFeedback
                onPress={props.onPress}
            >
                <View
                    style={{ height: specialHeight, width: specialWidth}}
                >
                    <Image
                        source={props.source}
                        style={[props.style, (props.rounded && {borderRadius: specialHeight / 2}), styles.imageStyle]}
                    />
                </View>
            </TouchableWithoutFeedback>
        )
    }
}

const styles = StyleSheet.create({
    imageStyle:{
        height: '100%',
        width: '100%',
        resizeMode: 'contain',
        backgroundColor: '#B3B6B9'
    },
})

export default Avatar