import React from 'react'
import {
    View,
    Text,
    TouchableOpacity,
    Image,
    StyleSheet,
} from 'react-native'

function Header(props){
    return(
        <View
            style={[styles.header, props.style]}
        >
            {
                props.back ? (
                    <TouchableOpacity
                        style={styles.headerButton}
                        onPress={props.onPress}
                    >
                        <Image
                            source={require('../../assets/icons/atras.png')}
                            style={styles.headerBackImage}
                        />
                    </TouchableOpacity>
                ) : (
                    <View style={styles.headerButton}></View>
                )
            }
            <View
                style={styles.headerTextBox}
            >
                <Text
                    style={styles.headerText}
                >
                    {props.title}
                </Text>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    header: {
        width: '100%',
        height: 35,
        flexDirection: 'row',
        marginTop: 10,
    },
    headerBackImage:{
        marginLeft: 10,
        height: 30,
        width: 30,
        resizeMode: 'contain',
    },
    headerButton:{
        height: 40,
        width: 60,
    },
    headerTextBox:{
        width: '65%'
    },
    headerText:{
        fontSize: 24,
        fontWeight: '300',
        alignSelf: 'center'
    },
})

export default Header