import React from 'react'
import {
    StyleSheet
} from 'react-native'
import PickerSelect from 'react-native-picker-select';

function PickerMonth(props){   
    state = { 
        meses: [
            {
                label: 'Enero',
                value: '01',
            },
            {
                label: 'Febrero',
                value: '02',
            },
            {
                label: 'Marzo',
                value: '03',
            },
            {
                label: 'Abril',
                value: '04',
            },
            {
                label: 'Mayo',
                value: '05',
            },
            {
                label: 'Junio',
                value: '06',
            },
            {
                label: 'Julio',
                value: '07',
            },
            {
                label: 'Agosto',
                value: '08',
            },
            {
                label: 'Septiembre',
                value: '09',
            },
            {
                label: 'Octubre',
                value: '10',
            },
            {
                label: 'Noviembre',
                value: '11',
            },
            {
                label: 'Diciembre',
                value: '12',
            }
        ]
    }
    
    return(
        <PickerSelect
            hideIcon={true}
            placeholder={{
                label: 'Mes',
                value: '',
            }}
            items={this.state.meses}
            onValueChange={props.onValueChange}
            style={{ ...pickerSelectStyles }}
            value={props.value}
            disabled={props.disabled}
        />
    )
}

const pickerSelectStyles = StyleSheet.create({
    inputIOS: {
        color: 'black',
        alignContent: 'center',
        minWidth: 50,
        maxWidth: 120,
        /*
        borderTopWidth: 0.8,
        borderTopColor: '#449cd0',
        borderLeftWidth: 0.8,
        borderLeftColor: '#449cd0',
        borderRightWidth: 0.8,
        borderRightColor: '#449cd0',
        borderBottomWidth: 0.8,
        borderBottomColor: '#449cd0',
        */
        marginBottom: 10,
        fontSize: 20,
        borderRadius: 5,
        marginLeft: 5,
        marginRight: 5,
        marginTop: 10,
        textAlign: 'center'
    },
    inputAndroid: {
        color: 'black',
        alignContent: 'center',
        minWidth: 50,
        maxWidth: 120,
        /*
        borderTopWidth: 0.8,
        borderTopColor: '#449cd0',
        borderLeftWidth: 0.8,
        borderLeftColor: '#449cd0',
        borderRightWidth: 0.8,
        borderRightColor: '#449cd0',
        borderBottomWidth: 0.8,
        borderBottomColor: '#449cd0',
        */
        marginBottom: 10,
        fontSize: 20,
        borderRadius: 5,
        marginLeft: 5,
        marginRight: 5,
        marginTop: 10,
        textAlign: 'center'
    },
});

export default PickerMonth