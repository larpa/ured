import React from 'react'
import { 
    View,
    Text,
    StyleSheet
} from 'react-native'

import TextInput from './text-input'

function TextInputIcon(props){
    return (
        <View style={[styles.inputContainer, props.style]}>
            <TextInput
                placeholder={props.placeholder}
                password={props.password}
                style={[styles.inputText, props.inputStyle]}
                onChangeText={props.onChangeText}
                returnKeyType={props.returnKeyType}
                value={props.value}
                keyboardType={props.keyboardType}
                autoCapitalize={props.autoCapitalize}
            />
        </View>
    )
}

const styles = StyleSheet.create({
    inputContainer:{
        // flexDirection: 'row',
        borderColor: 'red',
        borderWidth: 1,
    },
    inputText:{
        height:37,
        // width: '100%',
		
		// marginBottom: 24,
		// color:'#999999',
		// fontWeight: '600',
		// borderBottomColor: '#989898',
		// borderBottomWidth: 1,
        // backgroundColor: '#C1C1C1',
    }
})

export default TextInputIcon