// Parametros que recibe este componente:
//  por ahora solo...

// style => para estilos extras como margenes y paddings


import React from 'react'
import { 
    View,
    Text,
    TouchableHighlight,
    Image,
    StyleSheet
 } from "react-native"

function ImageAttachBox(props){
    return(
        <View style={[styles.container, props.style]}>
            {
                props.addFile ?
                    <TouchableHighlight
                        onPress={props.onPress}
                        style={styles.addSource}
                    >
                        <Image
                            style={styles.addSourceIcon}
                            source={require('../../assets/icons/nuevo-evento.png')}
                        />
                    </TouchableHighlight>
                :
                    <TouchableHighlight
                        onPress={props.onPress}
                    >
                        <View
                            style={styles.addSource}
                        >
                            <View
                                style={styles.holderDeleteIcon}
                            >
                                <Image
                                    style={styles.deleteIcon}
                                    source={require('../../assets/icons/nuevo-evento.png')}
                                />
                            </View>
                            <Image
                                style={styles.preview}
                                source={props.source ? {uri: props.source} : require('../../assets/empty.png') }
                            />
                        </View>
                    </TouchableHighlight>
            }
        </View>
    )
}

const styles = StyleSheet.create({
    container:{
        width: 64,
        height: 64,
        marginRight: 15,
        borderColor: '#4A3DDD',
        borderWidth: 2,
    },
    addSource:{
        position: 'absolute',
        top: 0,
        right: 0,
        width: 60,
        height: 60,
        backgroundColor: 'rgba(28, 28, 28, 0.2)',
        alignItems: 'center',
		justifyContent: 'center',
    },
    holderDeleteIcon:{
        width: 60,
        height: 60,
        backgroundColor: 'rgba(0, 0, 0, 0.6)',
        alignItems: 'center',
		justifyContent: 'center',
    },
    deleteIcon:{
        width: 45,
        height: 45,
        resizeMode: 'contain',
        transform: [
            { rotate: '45deg'}
        ],
    },
    addSourceIcon:{
        width: 45,
        height: 45,
        resizeMode: 'contain',
    },
    preview:{
        position: 'absolute',
        top: 0,
        left: 0,
        width: 60,
        height: 60,
        flex: 1,
        zIndex: -1,
    },
})

export default ImageAttachBox