// Parametros que recibe este componente:
// onPress => para manejar cuando se apreta el boton
// style => para agregar estilos como margen o cosas así...
// title => el texto que tiene dentro el boton

import React from 'react'
import {
    TouchableOpacity,
    Text,
    ActivityIndicator,
    StyleSheet
} from 'react-native'

function Button(props){
    return(
        <TouchableOpacity
            onPress={props.onPress}
        >
            {
                props.loading ?
                (
                    <ActivityIndicator size="small" color="#191919"/>
                ) :
                (
                    props.component ? (
                        props.children
                    ) : (
                        <Text
                            style={[props.style, styles.buttonText]}
                        >
                            {props.title}
                        </Text>
                    )
                )
            }
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    button:{
        padding: 10,
        backgroundColor: '#106EA8',
        borderRadius: 30,
        alignItems: 'center',
    },
    buttonText:{
        color: 'white',
        fontSize: 25,
        fontWeight: '400'
    }
})

export default Button