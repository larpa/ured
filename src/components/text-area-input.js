// Parametros que recibe este componente:

// placeholder => El texto de placeholder del input text
// password => mandar true si es un input de clave
// onChangeText => para manejar cada que se apreta una tecla
// returnKeyType => la palabra que debe llevar el boton de return del teclado 
// value => el valor del componente
//  style => los estilos extra que se necesiten

// numberOfLines => cantidad de lineas del textarea

import React from 'react'
import {
    TextInput,
    StyleSheet
} from 'react-native'

function InputText(props){
    return(
        <TextInput
            keyboardAppearance={props.keyboardAppearance}
            placeholder={props.placeholder}
            secureTextEntry={props.password}
            style={[styles.inputText, props.style]}
            onChangeText={props.onChangeText}
            returnKeyType={props.returnKeyType}
            value={props.value}
            multiline={true}
            numberOfLines={props.numberOfLines}
            blurOnSubmit={props.blurOnSubmit}
        />
    )
}

const styles = StyleSheet.create({
    inputText:{
        borderBottomWidth: 0.8,
        borderBottomColor: '#999999',
        marginBottom: 30,
        fontSize: 20
    },
})

export default InputText