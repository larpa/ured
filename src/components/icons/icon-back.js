import React from 'react'
import {
    Image,
    View,
    StyleSheet
} from 'react-native'

function Icon(props){
    return(       
        <Image 
            style={styles.icon}
            source={require('../../../assets/icons/atras.png')}
        />
    )
}

const styles = StyleSheet.create({
    icon: {
        height: '100%',
        width: '100%',
    }
})

export default Icon