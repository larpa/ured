import React from 'react'
import {
    Image,
    StyleSheet
} from 'react-native'

function Icon(props){
    return(
        <Image 
            style={styles.icon}
            source={require('../../../assets/icons/notificaciones.png')}
        />
    )
}

const styles = StyleSheet.create({
    icon: {
        height: 35,
        width: 35,
    }
})

export default Icon