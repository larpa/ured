import React from 'react'
import {
    Image,
    StyleSheet
} from 'react-native'

function Icon(props){
    return(
        <Image 
            style={styles.icon}
            source={require('../../../assets/icons/favorito-marcado.png')}
        />
    )
}

const styles = StyleSheet.create({
    icon: {
        height: '100%',
        width: '100%',
    }
})

export default Icon