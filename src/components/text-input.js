// Parametros que recibe este componente:

// placeholder => El texto de placeholder del input text
// password => mandar true si es un input de clave
// onChangeText => para manejar cada que se apreta una tecla
// returnKeyType => la palabra que debe llevar el boton de return del teclado 
// value => el valor del componente
//  style => los estilos extra que se necesiten
//  autoCapitalize => acepta:
                            // {
                            //     characters => all characters,
                            //     words => first letter of each word,
                            //     sentences => first letter of each sentence (default),
                            //     none => don't auto capitalize anything
                            // } 


// este componente es una clase porque asi se pueden manejar las referencias

import React, { Component } from 'react'
import {
    TextInput,
    StyleSheet,
    Platform
} from 'react-native'

class InputText extends Component{
    render(){
        return(
            <TextInput
                placeholder={this.props.placeholder}
                secureTextEntry={this.props.password}
                style={[styles.inputText, this.props.style]}
                onChangeText={this.props.onChangeText}
                returnKeyType={this.props.returnKeyType}
                value={this.props.value}
                onFocus={this.props.onFocus}
                onTouchStart={this.props.onTouchStart}
                keyboardType={this.props.keyboardType}
                ref={this.props.setRef}
                multiline={this.props.multiline}
                numberOfLines={this.props.numberOfLines}
                onBlur={this.props.onBlur}
                autoCapitalize={this.props.autoCapitalize}
            />
        )
    }
}

InputText.defaultProps = {
    ref: () => {},
  };

const styles = StyleSheet.create({
    inputText:{
        fontSize: 18,
        paddingTop: 8,
        paddingBottom: 8,
        paddingLeft: 10,
        backgroundColor: '#c1c1c1',
        borderRadius: 10,
    },
})

export default InputText