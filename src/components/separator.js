import React from 'react'
import {
    View,
    StyleSheet
} from 'react-native'

function Separator(props){
    return(
        <View style={[styles.separator, props.style]}>

        </View>
    )
}

const styles = StyleSheet.create({
    separator:{
        borderTopWidth: 1,
        borderTopColor: '#999999',
        marginTop: 10,
        marginBottom: 10,
    }
})

export default Separator