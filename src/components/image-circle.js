import React, { Component } from 'react'
import {
    TouchableOpacity,
    View,
    Text,
    StyleSheet,
    Image,
    ActivityIndicator,
} from 'react-native'

function ImageCircle(props){
        return(
            <TouchableOpacity 
                style={ [styles.container, props.style] }
                onPress={props.onPress}
            >
                <Image 
                    style={styles.image} 
                    source={props.url ? {uri: props.url} : require('../../assets/profile-pic-blank.png')}
                />
                {
                    props.loading ? 
                        <View
                            style={styles.loadingContainer}
                        >
                            <ActivityIndicator
                                style={styles.loadingComponent}
                                size="small"
                                color="#757779"
                            />
                        </View>
                        :
                        props.editView ? 
                            <View
                                style={styles.editContainer}
                            >
                                <Text>icon</Text>
                            </View>
                        :
                            <View></View>
                }
            </TouchableOpacity>   
        )
}

const styles = StyleSheet.create({
    container:{
        borderWidth:1,
        borderColor:'rgba(0,0,0,0.2)',
        alignItems:'center',
        justifyContent:'center',
        width:100,
        height:100,
        backgroundColor:'#fff',
        borderRadius:100,
    },
    loadingContainer:{
        position: 'absolute',
        width:130,
        height:130,
        alignItems:'center',
        justifyContent:'center',
        borderRadius:100,
        backgroundColor:'rgba(0, 0, 0, 0.6)',
    },
    loadingComponent:{
        position: 'absolute',
    },
    editContainer:{
        position: 'absolute',
        width:130,
        height:130,
        alignItems:'center',
        justifyContent:'center',
        borderRadius:100,
        backgroundColor:'rgba(0, 0, 0, 0.4)',
    },
    image: {
        height:128,
        width: 128,
        borderRadius: 64
    },
})

export default ImageCircle