import React from 'react'
import {
    StyleSheet
} from 'react-native'
import PickerSelect from 'react-native-picker-select';

function PickerCiudades(props){   
    state = { 
        ciudades: [
            {label: 'Lambayeque', value: 1},
            {label: 'Amazonas', value: 2},
            {label: 'Ancash', value: 3},
            {label: 'Apurimac', value: 4},
            {label: 'Arequipa', value: 5},
            {label: 'Ayacucho', value: 6},
            {label: 'Cajamarca', value: 7},
            {label: 'Callao', value: 8},
            {label: 'Cusco', value: 9},
            {label: 'Huancavelica', value: 10},
            {label: 'Huanuco', value: 11},
            {label: 'Ica', value: 12},
            {label: 'Junin', value: 13},
            {label: 'La Libertad', value: 14},
            {label: 'Lima', value: 15},
            {label: 'Loreto', value: 16},
            {label: 'Madre de Dios', value: 17},
            {label: 'Moquegua', value: 18},
            {label: 'Pasco', value: 19},
            {label: 'Piura', value: 20},
            {label: 'Puno', value: 21},
            {label: 'San Martín', value: 22},
            {label: 'Tacna', value: 23},
            {label: 'Tumbes', value: 24}
        ]
    }
    
    return(
        <PickerSelect
            hideIcon={true}
            placeholder={{
                label: 'Seleccione una ciudad',
                value: 0,
            }}
            items={this.state.ciudades}
            onValueChange={props.onValueChange}
            style={{ ...pickerSelectStyles }}
            value={props.value}
            disabled={props.disabled}
        />
    )
}
const pickerSelectStyles = StyleSheet.create({
    inputIOS: {
        fontSize: 20,
        borderWidth: 1,
        borderColor: 'white',
        color: 'black',
        borderBottomColor: '#999999',
        borderBottomWidth: 0.7,
        marginBottom: 10,
    },
    inputAndroid: {
        fontSize: 20,
        borderWidth: 1,
        borderColor: 'white',
        color: 'black',
        borderBottomColor: '#999999',
        borderBottomWidth: 0.7,
        marginBottom: 10,
    },
});

export default PickerCiudades