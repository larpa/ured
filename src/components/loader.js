import React, { Component } from 'react';
import {
  StyleSheet,
  Image,
  View,
  Text,
  Modal,
  ActivityIndicator
} from 'react-native';

function Loader(props){
  return(
    <Modal
      transparent={true}
      animationType={'none'}
      visible={props.visible}>
      <View style={styles.modalBackground}>
        <View style={styles.activityIndicatorWrapper}>
          <Text
            style={styles.logo}
          >
            URED
          </Text>
          <ActivityIndicator
            animating={props.visible} 
          />
          <Text style={styles.texto}>Cargando</Text>
        </View>
      </View>
    </Modal>
  )
}

const styles = StyleSheet.create({
  modalBackground: {
    flex: 1,
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'space-around',
    backgroundColor: '#00000085'
  },
  activityIndicatorWrapper: {
    backgroundColor: '#FFFFFF',
    height: 200,
    width: 250,
    borderRadius: 10,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-around'
  },
  logo:{
    fontSize: 40,
		fontWeight: '600',
		color: '#FF2C2D',
  },
  texto: {
    fontSize: 20
  }
});

export default Loader;