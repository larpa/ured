import React from 'react'
import {
    View,
    Text,
    StyleSheet
} from 'react-native'

import TextInput from './text-input'

function Search(props){
    return (
        <View
            style={[styles.box, props.style]}
        >
            <TextInput
                style={styles.text}
                onChangeText={props.onChange}
                value={props.text}
                placeholder={props.placeholder ? props.placeholder : 'Buscar...' }
            />
        </View>
    )
}

const styles = StyleSheet.create({
    box:{
        marginLeft: 15,
        marginRight: 15,
        borderColor: '#343434',
        borderRadius: 16,
        backgroundColor: 'white',
        shadowOffset:{  width: 2,  height: 3,  },
        shadowColor: 'black',
        shadowOpacity: 0.3,
    },
    text:{
        fontSize: 20,
        fontWeight: '200',
        color: '#343434'
    },
})

export default Search