import React from 'react'
import {
    TextInput,
    StyleSheet
} from 'react-native'

function InputDate(props){
    return(
        <TextInput
            placeholder={props.placeholder}
            keyboardType='numeric'
            secureTextEntry={props.password}
            style={[styles.inputDate, props.style]}
            onChangeText={props.onChangeText}
            returnKeyType={props.returnKeyType}
            value={props.value}
        />
    )
}

const styles = StyleSheet.create({
    inputDate:{
        alignContent: 'center',
        width:60,
        borderTopWidth: 0.8,
        borderTopColor: '#449cd0',
        borderLeftWidth: 0.8,
        borderLeftColor: '#449cd0',
        borderRightWidth: 0.8,
        borderRightColor: '#449cd0',
        borderBottomWidth: 0.8,
        borderBottomColor: '#449cd0',
        marginBottom: 10,
        fontSize: 20,
        borderRadius: 5,
        marginLeft: 5,
        marginRight: 5,
        marginTop: 20,
        textAlign: 'center'
    },
})

export default InputDate