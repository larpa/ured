import React from 'react'
import {
    StyleSheet
} from 'react-native'
import PickerSelect from 'react-native-picker-select';

function PickerSexo(props){   
    state = { 
        sexo: [
            {
                label: 'Masculino',
                value: 'M',
            },
            {
                label: 'Femenino',
                value: 'F',
            },
        ]
    }
    
    return(
        <PickerSelect
            hideIcon={true}
            placeholder={{
                label: 'Selecciona tu sexo',
                value: '',
            }}
            items={this.state.sexo}
            onValueChange={props.onValueChange}
            style={{ ...pickerSelectStyles }}
            value={props.value}
            disabled={props.disabled}
        />
    )
}
const pickerSelectStyles = StyleSheet.create({
    inputIOS: {
        fontSize: 20,
        borderWidth: 1,
        borderColor: 'white',
        color: 'black',
        borderBottomColor: '#999999',
        borderBottomWidth: 0.7,
        //marginTop:30
    },
    inputAndroid: {
        fontSize: 20,
        borderWidth: 1,
        borderColor: 'white',
        color: 'black',
        borderBottomColor: '#999999',
        borderBottomWidth: 0.7,
        //marginTop:30
    },
});

export default PickerSexo