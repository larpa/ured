// React dependencies
import React, { Component } from 'react'
import {
	View,
	Text,
	Image,
	TouchableWithoutFeedback,
	SafeAreaView,
	ScrollView,
	StyleSheet
} from 'react-native'
import { connect } from 'react-redux'
import { NavigationActions } from 'react-navigation'

// Our dependencies
import API from '../../../utils/api'

// External dependencies
import Dimensions from 'Dimensions';

// our modules
import VerticalSeparator from '../components/vertical-separator'
import Button from '../../components/button'
import TextInput from '../../components/text-input'
import Loader from '../../components/loader'
import showAlert from '../../../utils/alert'

// external modules
import api from '../../../utils/api';

const {
	width,
	height
} = Dimensions.get('window')

// const cardHeight = height / 1.6;
const cardHeight = height / 1.8;
const cardWidth = width/1.1;
const inputWidth = width/1.4;
const inputWidthBtn = width/1.5;

class LoginForm extends Component{
	static navigationOptions = () => {
		return {
			header: null
		}
	}
	state = {
		mail: '',
		password: '',
		isLoading: false,
	}

	handleOnChangeCorreo = text => {
		this.setState({
			mail: text
		})
	}

	handleOnChangeClave = text => {
		this.setState({
			password: text
		})
	}

	launchLogin = async () => {
		var {mail,password} = this.state
		this.setState({ isLoading: true });

		mail = mail.replace("&nbsp;", "");
		mail = mail == undefined ? "" : mail;

		password = password.replace("&nbsp;", "");
		password = password == undefined ? "" : password;

		const validarCorreo = /^[-\w.%+]{1,64}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/i

		if (!mail || 0 === mail.trim().length || !password || 0 === password.trim().length) {
			this.setState({ isLoading: false });
			showAlert('Completa los datos', 'warning', 'short')
		}else{
			if(!validarCorreo.test(mail)){
				this.setState({ isLoading: false });
				showAlert('El correo no tiene el formato correcto', 'warning', 'short')
			}else{
				const userData = await API.login(mail.toLowerCase(), password)
				if(userData.success){
					this.props.dispatch({
						type: 'SET_USER',
						payload:{
							user: userData.response
						}
					})
					this.props.dispatch(NavigationActions.navigate({
						routeName: 'NewsList'
					}))
				}else{
					this.setState({ isLoading: false });
					showAlert(userData.response, 'error', 'short')
				}

			}
		}
	}

	render(){
		return(
			<SafeAreaView>
				<ScrollView>
					<Loader
						visible={this.state.isLoading}
					/>
					<View style={styles.screen}>
						<View style={styles.card}>
							<Text
								style={styles.logo}
							>
								URED
							</Text>

							<Text
								style={styles.label}
							>
								Correo:
							</Text>
							<TextInput
								style={styles.inputText}
								onChangeText={this.handleOnChangeCorreo}
								value={this.state.mail}
								returnKeyType="next"
								keyboardType="email-address"
								autoCapitalize="none"
							/>
							<Text
								style={styles.label}
							>
								Clave:
							</Text>
							<TextInput
								style={styles.inputText}
								onChangeText={this.handleOnChangeClave}
								value={this.state.password}
								returnKeyType="done"
								password={true}
							/>
							<Button
								component
								onPress={this.launchLogin}
							>
								<View
									style={styles.buttonBox}
								>
									<Text
										style={styles.buttonText}
									>
										iniciar sesión
									</Text>
								</View>
							</Button>
						</View>
					</View>
				</ScrollView>
			</SafeAreaView>
		)
	}
}

const styles = StyleSheet.create({
	screen:{
		height: height,
		width: width,
		alignItems: 'center',
		justifyContent: 'center',
	},
	card:{
		marginTop: '-20%',
		backgroundColor: '#E9E9E9',
		borderRadius: 20,
		width: cardWidth,
		height: cardHeight,
		maxHeight: 350,
		alignItems: 'center',
		justifyContent: 'center',
		shadowOffset:{
            width: 2,
            height: 3,
        },
        shadowColor: 'black',
        shadowOpacity: 0.3,
	},
	logo:{
		fontSize: 40,
		fontWeight: '600',
		color: '#FF2C2D',
	},
	inputText:{
		width: inputWidth,
		marginTop: 10,
	},
	label:{
		width: inputWidth,
		fontSize: 14,
		marginTop: 15,
	},
	buttonBox:{
		width: inputWidthBtn,
		marginTop: 30,
		padding: 10,
		alignItems: 'center',
		borderRadius: 10,
		backgroundColor: '#4A3DDD',
	},
	buttonText:{
		fontSize: 18,
		color: 'white',
		fontWeight: '600',
	},
})

export default connect(null)(LoginForm)