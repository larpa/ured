// Agregar recordatorios de reuniones
// Alerta de eventos internos de la universidad  como por ejemplo
//          - Franja de formación
//          - Capacitaciones
import React from 'react'
import {
    Text,
} from 'react-native'
import {
    createStackNavigator,
    createBottomTabNavigator,
    createSwitchNavigator,
    createAppContainer,
} from 'react-navigation'

// Components
import Loading from './screens/containers/loading'
import Login from './login/containers/login-form'

import Profile from './profile/containers/profile-view'
import NewsList from './news/containers/news-list'
import New from './news/containers/new'
import Notifications from './notification/containers/notification-list'
import BurgerMenu from './burger-menu/containers/burger-menu'
import MenuList from './menu/containers/menu-list'
import AddMenuItem from './menu/containers/add-menu-item'
import MisSolicitudes from './solicitudes/containers/mis-solicitudes'
import Solicitudes from './solicitudes/containers/solicitudes'
import AddSolicitud from './solicitudes/containers/add-solicitud'
import MiSolicitud from './solicitudes/containers/mi-solicitud'
import Solicitud from './solicitudes/containers/solicitud'
import ColaboradorMesList from './colaborador-mes/containers/colaborador-mes-list'
import AddColaboradorMes from './colaborador-mes/containers/add-colaborador-mes'
import Contactos from './contactos/containers/contactos-list'
import ErrorList from './errors/containers/error-list'
import AddError from './errors/containers/add-error'


// Utils
import hapticFeedback from '../utils/hepatic-feedback';

const TabMainNavigator = createBottomTabNavigator({
    NewsList: {
        screen: NewsList,
        navigationOptions:{
            tabBarIcon: <Text>🏠</Text>,
            tabBarOnPress: ({ navigation, defaultHandler }) => {
                hapticFeedback('impactHeavy')
                defaultHandler();
            }
        }
    },
    Notifications: {
        screen: Notifications,
        navigationOptions:{
            tabBarIcon: <Text>📫</Text>,
            tabBarOnPress: ({ navigation, defaultHandler }) => {
                hapticFeedback('impactHeavy')
                defaultHandler();
            }
        }
    },
    Profile: {
        screen: Profile,
        navigationOptions:{
            tabBarIcon: <Text>👦🏻</Text>,
            tabBarOnPress: ({ navigation, defaultHandler }) => {
                hapticFeedback('impactHeavy')
                defaultHandler();
            }
        }
    },
    BurgerMenu: {
        screen: BurgerMenu,
        navigationOptions:{
            tabBarIcon: <Text>📦</Text>,
            tabBarOnPress: ({ navigation, defaultHandler }) => {
                hapticFeedback('impactHeavy')
                defaultHandler();
            }
        }
    },
},{
    initialRouteName: 'NewsList',
    tabBarOptions:{
        showLabel: false,
        activeBackgroundColor: '#EBFAFE',
    },
}
)

const MainApp = createStackNavigator({
    main: TabMainNavigator,
    New: New,
    MenuList: MenuList,
    AddMenuItem: AddMenuItem,
    MisSolicitudes: MisSolicitudes,
    Solicitudes: Solicitudes,
    AddSolicitud: AddSolicitud,
    Solicitud: Solicitud,
    MiSolicitud: MiSolicitud,
    ColaboradorMesList: ColaboradorMesList,
    AddColaboradorMes: AddColaboradorMes,
    Contactos: Contactos,
    ErrorList: ErrorList,
    AddError: AddError,
},{
    headerMode: 'none',
    mode: 'card',
    initialRouteName: 'main',
})

const NonLoginApp = createStackNavigator({
    Login: Login,
},{
    initialRouteName: 'Login',
    headerMode: 'none',
    mode: 'card',
})

const SwitchNavigator = createSwitchNavigator({
    App: MainApp,
    Loading: Loading,
    NonLoginApp: NonLoginApp,
},{
    initialRouteName: 'Loading',
    headerMode: 'none',
})

const appContainer = createAppContainer(SwitchNavigator)

export default appContainer