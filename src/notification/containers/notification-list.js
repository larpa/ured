import React, { Component } from 'react'
import {
    Text,
    View,
    FlatList,
    RefreshControl,
    StyleSheet,
    Alert
} from 'react-native'
import { connect } from 'react-redux'
import { NavigationActions } from 'react-navigation'

import NotificationListLayout from '../components/notification-list-layout'
import NotificationItem from '../components/notification-item'
import Header from '../../components/header'
import Loader from '../../components/loader'
import Separator from '../../components/separator'
import EmptyNotificationList from '../components/empty-notification-list'

import API from '../../../utils/api'

class Notifications extends Component{
    state = {
        refreshing: false,
	}

    renderItem = ({item}) => <NotificationItem {...item} onPress={this.openMessage.bind(null, item.id, item.id_tipo, item.id_evento)}/>
    keyExtractor = item => item.id.toString()
    renderItemSeparator = () => <Separator style={{marginLeft: 20, marginRight: 20,}}/>
    renderEmptyComponent = () => <EmptyNotificationList text="Aun no tienes notificaciones..."/>

    openMessage = (id, tipo, id_evento) => {
        if(tipo == 3){
            this.props.dispatch(NavigationActions.navigate({
                routeName: 'SingleEvent',
                params: {
                    id_selected: id_evento
                }
            }))
        }
    }

    loadNotifications = async () => {
        this.setState({
            refreshing: true
        })
        // if(this.props.user.id){
        //     const listData = await API.listNotifications(this.props.user.id)
        //     if(listData.success){
        //         this.props.dispatch({
        //             type: 'LOAD_NOTIFICATIONS',
        //             payload: {
        //                 notifications: listData.response
        //             }
        //         })
        //     }
        // }

        setTimeout(()=>{
            this.setState({
                refreshing: false
            });
        }, 2000)

    }

    handleRefresh = () => {
        this.loadNotifications()
    }

    componentDidMount(){
        this.loadNotifications()
    }

    render(){
        return(
            <NotificationListLayout>
                <Header
                    title="Notificaciones"
                />
                <FlatList
                    keyExtractor={this.keyExtractor}
                    data={this.props.notificationList}
                    ListEmptyComponent={this.renderEmptyComponent}
                    ItemSeparatorComponent={this.renderItemSeparator}
                    renderItem={this.renderItem}
                    refreshControl={<RefreshControl refreshing={this.state.refreshing} onRefresh={this.handleRefresh}/>}
                />
            </NotificationListLayout>
        )
    }
}

const styles = StyleSheet.create({
    header:{
        marginTop: 10,
    }
})

function mapStateToProps(state, props){
    let noLi = null;
    if(!state.notification){
        noLi = [];
    }else{
        noLi = state.notification.notificationList
    }
    return {
        user: state.user[0],
        notificationList: noLi,
    }
}

export default connect(mapStateToProps)(Notifications)