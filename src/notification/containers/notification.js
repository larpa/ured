import React, { Component } from 'react';
import { 
  View, 
  Text, 
  StyleSheet,
  Image,
 } from 'react-native';
import { connect } from 'react-redux'

import Header from '../../components/header'
import NotificationLayout from '../components/notification-layout'
import ImageCirle from '../../components/image-circle'
// import { CachedImage } from 'react-native-cached-image'

import { NavigationActions } from 'react-navigation'

class Notification extends Component{
  state = {
    notificationSelected: {}
  }

  componentWillMount = () => {
    this.setState({
      notificationSelected: this.props.notification.find(obj => {
        return obj.id === this.props.navigation.getParam('notificationId', '0')
      })
    })
  }

  handleBack = () => {
    this.props.dispatch(NavigationActions.back())
  }

  render() {
    return (
      <NotificationLayout>
        <Header
          title="Notificación"
          back
          onPress={this.handleBack}
        />
          <View style={styles.container}>
              <Image
                  style={styles.image}
                  source={this.state.notificationSelected.url_logo ? {uri: this.state.notificationSelected.url_logo} : require('../../../assets/empty.png')}
              />
             <View style={styles.containerDatos}>
                <Text style={styles.datos}>
                  {this.state.notificationSelected.nombre}
                </Text>
                {/* <Text style={styles.datos}>
                  Persona remitente
                </Text> */}
              </View>
          </View>
          <View style={styles.container2}>
            {/* <Text style={styles.comunicadoTitle}>
              Comunicado
            </Text> */}
            <Text style={styles.comunicadoDescripcion}>
              {this.state.notificationSelected.descripcion}
            </Text>
          </View>
      </NotificationLayout>
    );
  }
}


const styles = StyleSheet.create({
  container:{
    padding: 20,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
  },
  container2:{
    padding: 20,
    flexDirection: 'column',
  },
  containerDatos:{
    paddingLeft: 30,
    flexDirection: 'column',
  },
  datos:{
    marginTop: 10,
    fontSize: 15,
    fontWeight: '300'
  },
  comunicadoTitle:{
    marginTop: 15,
    fontSize: 35,
    fontWeight: '300'
  },
  comunicadoDescripcion:{
    marginTop: 5,
    fontSize: 15,
    fontWeight: '300',
    textAlign: 'justify'
  },
  image:{
    alignItems:'center',
    justifyContent:'center',
    width:100,
    height:100,
    borderRadius:50,
  },
})

function mapStateToProps(state, props){
  return{
    notification: state.notification.notificationList
  }
}

export default connect(mapStateToProps)(Notification)