import React from 'react'
import {
    View,
    SafeAreaView,
    StyleSheet,
} from 'react-native'

import Dimensions from 'Dimensions';

const {
	width, 
	height
} = Dimensions.get('window')

function NotificationListLayout(props){
    return(
        <SafeAreaView>
            <View style={styles.container}>
                {props.children}
            </View>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container:{
        height: height,
    }
})

export default NotificationListLayout