import React from 'react'
import {
    View,
    Text,
    Image,
    TouchableOpacity,
    StyleSheet,
} from 'react-native'

import Dimensions from 'Dimensions';

const {
	width, 
	height
} = Dimensions.get('window')

function MessageItem(props){
    return(
        <TouchableOpacity 
            style={styles.container}
            onPress={props.onPress}
        >
            <Image
                style={styles.profilePic}
                source={{uri:props.url_logo}}
            />
            <View style={styles.messageTextBox}>
                <Text style={styles.name}>
                    {props.nombre}
                </Text>
                <Text 
                    style={styles.lastMessage}
                    // adjustsFontSizeToFit={true}
                    numberOfLines={2}
                > 
                    {props.descripcion}
                </Text>
            </View>
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    container:{
        flexDirection: 'row',
        margin: 10,
        
    },
    profilePic:{
        height: 70,
        width: 70,
        borderRadius: 70/2,
    },
    messageTextBox:{
        marginLeft: 20,

    },
    lastMessage:{
        marginTop: 5,
        width: (width /1.5 ),
    },
    name:{
        fontSize: 18,
    },
})

export default MessageItem