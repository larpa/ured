import React, {Component} from 'react'
import {
    View,
    Text,
    SafeAreaView,
    FlatList,
    RefreshControl,
    StyleSheet,
} from 'react-native'
import { connect } from 'react-redux'

import { NavigationActions } from 'react-navigation'
import ActionButton from 'react-native-action-button'

import Header from '../../components/header'
import EmptyMenuList from '../components/empty-menu-list'
import MenuItem from '../components/menu-item'

import API from '../../../utils/api'
import showAlert from '../../../utils/alert';

class MenuList extends Component{

    state = {
        refreshing: false
    }

    keyExtractor = item => item.id.toString()

    renderEmptyComponent = () => <EmptyMenuList
                                    text="Aún no se registra el menú del día."
                                />

    renderItem = ({item}) =>    <MenuItem
                                    {...item}
                                />
    renderLastItem = () => <View style={{ height: 0, marginBottom: 150}} ></View>

    handleRefresh = () => {
        this.loadMenu()
    }

    loadMenu = async () => {

        this.setState({
            refreshing: true
        })

        const menuResponse = await API.getTodaysMenu()

        if(menuResponse.success){
            this.props.dispatch({
                type: 'LOAD_MENU_DATA',
                payload:{
                    menus: menuResponse.response
                }
            })
        }else{
            showAlert("Ocurrio un error cargando el menú", 'error', 'long')
        }

        this.setState({
            refreshing: false
        })

    }

    handleOnBack = () => {
        this.props.dispatch(NavigationActions.back())
    }

    handleAddMenu = () => {
        this.props.dispatch(NavigationActions.navigate({
            routeName: 'AddMenuItem'
        }))
    }

    componentDidMount() {
        this.loadMenu()
    }

    render(){
        return(
            <SafeAreaView>
                <View
                    style={styles.container}
                >
                    <Header
                        title="Cafetería"
                        back
                        onPress={this.handleOnBack}
                    />

                    <FlatList
                        style={styles.list}
                        keyExtractor={this.keyExtractor}
                        data={this.props.menu}
                        ListEmptyComponent={this.renderEmptyComponent}
                        renderItem={this.renderItem}
                        ListFooterComponent={this.renderLastItem}
                        refreshControl={<RefreshControl refreshing={this.state.refreshing} onRefresh={this.handleRefresh}/>}
                    />

                    {
                        true && <ActionButton
                                    style={styles.fab}
                                    buttonColor="rgba(231,76,60,1)"
                                    onPress={this.handleAddMenu}
                                />
                    }
                </View>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        height: '100%',
    },
    fab:{
        display: 'flex',
        marginTop: '90%'
    },
    list:{
        height: '100%'
    },
})

function mapStateToProps(state, props){
    return{
        menu: state.menu.menuList
    }
}

export default connect(mapStateToProps)(MenuList)