import React, { Component } from 'react'
import {
    View,
    Text,
    PickerIOS,
    SafeAreaView,
    StyleSheet,
    Button,
    Switch,
    ScrollView,
    KeyboardAvoidingView
} from 'react-native'

import { connect } from 'react-redux'
import { NavigationActions } from 'react-navigation'

import Header from '../../components/header'
import TextInput from '../../components/text-input'
import MyButton from '../../components/button'

import PickerSelect from 'react-native-picker-select'
import { BottomSheet } from 'react-native-btr'

import API from '../../../utils/api'
import showAlert from '../../../utils/alert'

class AddMenuItem extends Component{

    state = {
        menuList: [],
        menuSelected: 0,
        addModalVisible: false,
        addMenuText: '',
        addMenuPrice: '',
        registerButtonLoading: false,
        registerMenuButtonLoading: false,
        entrada: '',
        segundo: '',
        bebida: '',
        postre: '',
        switchEntrada: true,
        switchSegundo: false,
        switchPostre: false,
        switchBebida: false,
    }

    loadMenus = async () => {
        const menus = await API.getMenus()
        let holder = []
        if(menus.success && menus.response){
            menus.response.map(item => {
                holder.push({label: item.nombre, value: item.id, key: item.id})
            })
        }
        this.setState({
            menuList: holder
        })
    }

    componentDidMount() {
        this.loadMenus()
    }

    handleOnBack = () => {
        this.props.dispatch(NavigationActions.back())
    }

    handleOnMenuPickerChange = (item) => {
        this.setState({
            menuSelected: item
        })
    }

    handleNewMenu = () => {
        this.setState({
            addModalVisible: true
        })
    }

    closeAddModal = () => {
        this.setState({
            addModalVisible: false
        })
    }

    handleSwitchEntrada = () =>  {
        this.setState({
            switchEntrada: !this.state.switchEntrada
        })
    }

    handleSwitchSegundo = () =>  {
        this.setState({
            switchSegundo: !this.state.switchSegundo
        })
    }

    handleSwitchBebida = () =>  {
        this.setState({
            switchBebida: !this.state.switchBebida
        })
    }

    handleSwitchPostre = () =>  {
        this.setState({
            switchPostre: !this.state.switchPostre
        })
    }

    handleOnChangeAddMenuText = value => {
        this.setState({
            addMenuText: value
        })
    }

    handleOnChangeAddMenuPrice = value => {
        this.setState({
            addMenuPrice: value
        })
    }

    handleOnChangeEntrada = value => {
        this.setState({
            entrada: value
        })
    }

    handleOnChangeSegundo = value => {
        this.setState({
            segundo: value
        })
    }

    handleOnChangeBebida = value => {
        this.setState({
            bebida: value
        })
    }

    handleOnChangePostre = value => {
        this.setState({
            postre: value
        })
    }

    handleRegisterButton = async () => {

        if(this.state.addMenuText === '' || !Number(this.state.addMenuPrice)){
            showAlert('Debes completar todos los campos.', 'warning', 'error')
            return
        }

        this.setState({
            registerButtonLoading: true
        })

        const AddResponse = await API.addMenu(this.state.addMenuText, this.state.addMenuPrice, this.props.user.id)

        this.setState({
            registerButtonLoading: false
        })

        if(AddResponse.success){
            this.loadMenus()
            this.setState({
                addModalVisible: false,
                addMenuText: '',
                addMenuPrice: '',
            })
        }else{
            showAlert('Ocurrio un error, intentalo de nuevo.', 'error', 'long')
        }
    }

    handleRegisterMenuItemButton = async () => {

        if(this.state.menuSelected === 0){
            showAlert("Debes escoger a que menú pertenecen estos platos.", 'warning', 'long')
            return
        }

        if(!this.state.switchEntrada && !this.state.switchSegundo && !this.state.switchBebida && !this.state.switchPostre){
            showAlert("Debes registrar al menos una comida", 'warning', 'long')
            return
        }

        if((this.state.switchEntrada && this.state.entrada === "") || (this.state.switchSegundo && this.state.segundo === "") || (this.state.switchBebida && this.state.bebida === "") || (this.state.switchPostre && this.state.postre === "")){
            showAlert("Debes completar todos los campos", "warning", "long")
            return
        }

        this.setState({
            registerMenuButtonLoading: true
        })

        const entrada = this.state.entrada === '' ? '-' : this.state.entrada
        const segundo = this.state.segundo === '' ? '-' : this.state.segundo
        const bebida = this.state.bebida === '' ? '-' : this.state.bebida
        const postre = this.state.postre === '' ? '-' : this.state.postre

        const addItemResponse = await API.addItemMenu(this.state.menuSelected, entrada, segundo, bebida, postre, this.props.user.id)

        this.setState({
            registerMenuButtonLoading: false
        })

        if(addItemResponse.success){
            showAlert("Menú registrado!", "success", "long")
            this.props.dispatch(NavigationActions.back())
        }else{
            showAlert("Ocurrio un error, vuelve a intentarlo", "error", "long")
        }
    }

    render(){
        return(
            <SafeAreaView>
                <KeyboardAvoidingView
                    behavior="padding"
                >
                    <Header
                        title="Agregar al menú"
                        back
                        onPress={this.handleOnBack}
                    />
                    <ScrollView
                        style={styles.container}
                    >
                        <View>
                            <View
                                style={styles.doubleInputBox}
                            >
                                {
                                    this.state.menuSelected != 0 && <Text
                                                                        style={styles.doubleLabelInput}
                                                                    >
                                                                        Menú
                                                                    </Text>
                                }
                                <View
                                    style={styles.doubleInput}
                                >
                                    <PickerSelect
                                        hideIcon={true}
                                        placeholder={{
                                            label: 'Seleccione un menú',
                                            value: 0,
                                        }}
                                        items={this.state.menuList}
                                        onValueChange={(value) => {
                                            this.setState({
                                                menuSelected: value
                                            })
                                        }}
                                        style={{...specialStylesForPicket}}
                                        value={this.state.menuSelected}
                                    />
                                    <Button
                                        onPress={this.handleNewMenu}
                                        title="nuevo"
                                        color="#841584"
                                    />
                                </View>
                            </View>
                            <View
                                style={styles.inputBox}
                            >
                                <View
                                    style={styles.doubleLabel}
                                >
                                    <Text
                                        style={styles.labelInput}
                                    >
                                        Entrada
                                    </Text>

                                    <Switch
                                        onValueChange={this.handleSwitchEntrada}
                                        value={this.state.switchEntrada}
                                    />
                                </View>
                                {
                                    this.state.switchEntrada &&
                                    <TextInput
                                        style={styles.dinamicInput}
                                        onChangeText={this.handleOnChangeEntrada}
                                        value={this.state.entrada}
                                        returnKeyType="next"
                                    />
                                }
                            </View>
                            <View
                                style={styles.inputBox}
                            >
                                <View
                                    style={styles.doubleLabel}
                                >
                                    <Text
                                        style={styles.labelInput}
                                    >
                                        Segundo
                                    </Text>
                                    <Switch
                                        onValueChange={this.handleSwitchSegundo}
                                        value={this.state.switchSegundo}
                                    />
                                </View>
                                {
                                    this.state.switchSegundo &&
                                    <TextInput
                                        style={styles.dinamicInput}
                                        onChangeText={this.handleOnChangeSegundo}
                                        value={this.state.segundo}
                                        returnKeyType="next"
                                    />
                                }
                            </View>
                            <View
                                style={styles.inputBox}
                            >
                                <View
                                    style={styles.doubleLabel}
                                >
                                    <Text
                                        style={styles.labelInput}
                                    >
                                        Bebida
                                    </Text>
                                    <Switch
                                        onValueChange={this.handleSwitchBebida}
                                        value={this.state.switchBebida}
                                    />
                                </View>
                                {
                                    this.state.switchBebida &&
                                    <TextInput
                                        style={styles.dinamicInput}
                                        onChangeText={this.handleOnChangeBebida}
                                        value={this.state.bebida}
                                        returnKeyType="next"
                                    />
                                }
                            </View>
                            <View
                                style={styles.inputBox}
                            >
                                <View
                                    style={styles.doubleLabel}
                                >
                                    <Text
                                        style={styles.labelInput}
                                    >
                                        Postre
                                    </Text>
                                    <Switch
                                        onValueChange={this.handleSwitchPostre}
                                        value={this.state.switchPostre}
                                    />
                                </View>
                                {
                                    this.state.switchPostre &&
                                    <TextInput
                                        style={styles.dinamicInput}
                                        onChangeText={this.handleOnChangePostre}
                                        value={this.state.postre}
                                        returnKeyType="next"
                                    />
                                }
                            </View>

                            <MyButton
                                component
                                style={styles.registerButton}
                                color='#4A3DDD'
                                onPress={this.handleRegisterMenuItemButton}
                                loading={this.state.registerMenuButtonLoading}
                            >
                                <View
                                    style={styles.buttonBox}
                                >
                                    <Text
                                        style={styles.buttonText}
                                    >
                                        Registrar
                                    </Text>
                                </View>
                            </MyButton>
                        </View>


                        <BottomSheet
                                visible={this.state.addModalVisible}
                                onBackButtonPress={this.closeAddModal}
                                onBackdropPress={this.closeAddModal}
                        >
                            <View
                                style={styles.modalView}
                            >
                                <View
                                    style={{width: '100%', alignItems: 'center', marginBottom: 30,}}
                                >
                                    <Text
                                        style={{fontSize: 22, fontWeight: '400',}}
                                    >
                                        Registrar nuevo menú
                                    </Text>
                                </View>
                                <Text
                                    style={styles.labelModalInput}
                                >
                                    Menú
                                </Text>
                                <TextInput
                                    style={styles.inputText}
                                    onChangeText={this.handleOnChangeAddMenuText}
                                    value={this.state.addMenuText}
                                    returnKeyType="next"
                                />
                                <View style={{width: '100%', height: 20,}}></View>
                                <Text
                                    style={styles.labelModalInput}
                                >
                                    Precio
                                </Text>
                                <TextInput
                                    style={styles.inputText}
                                    onChangeText={this.handleOnChangeAddMenuPrice}
                                    value={this.state.addMenuPrice}
                                    returnKeyType="next"
                                    keyboardType="decimal-pad"
                                />
                                <View style={{width: '100%', height: 20,}}></View>
                                <MyButton
                                    component
                                    style={styles.registerButton}
                                    color='#4A3DDD'
                                    onPress={this.handleRegisterButton}
                                    loading={this.state.registerButtonLoading}
                                >
                                    <View
                                        style={styles.buttonBox}
                                    >
                                        <Text
                                            style={styles.buttonText}
                                        >
                                            Registrar
                                        </Text>
                                    </View>
                                </MyButton>
                            </View>
                        </BottomSheet>
                    </ScrollView>
                </KeyboardAvoidingView>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    menuPicker:{
        fontSize: 26,
    },
    container:{
        height: '100%',
    },
    doubleInputBox:{
        marginTop: 15,
    },
    inputBox:{
        marginLeft: 15,
        marginTop: 15,
    },
    labelInput:{
        fontSize: 20,
        color: '#757779',
    },
    labelModalInput:{
        fontSize: 18,
        color: '#757779'
    },
    doubleLabelInput:{
        fontSize: 18,
        color: '#757779',
        marginLeft: 15,
    },
    doubleInput:{
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginLeft: 15,
        marginRight: 15,
    },
    doubleLabel:{
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginRight: 15,
    },
    inputText:{
        marginTop: 10,
        width: '100%'
    },
    modalView: {
        backgroundColor: '#fff',
        width: '100%',
        height: '90%',
        borderTopLeftRadius: 25,
        borderTopRightRadius: 25,
        paddingLeft: 15,
        paddingRight: 15,
        paddingTop: 20,
    },
    registerButton:{
        marginLeft: 25,
        marginRight: 25,
        marginTop: 10,
        marginBottom: 10,
    },
    buttonBox:{
		width: '80%',
        marginTop: 10,
        marginLeft: '10%',
        marginBottom: 10,
		padding: 10,
		alignItems: 'center',
		borderRadius: 10,
		backgroundColor: '#4A3DDD',
    },
    buttonText:{
		fontSize: 18,
		color: 'white',
		fontWeight: '600',
    },
    dinamicInput:{
        marginTop: 10,
        marginBottom: 20,
        marginRight: 15,
    }
})

const specialStylesForPicket = StyleSheet.create({
    inputIOS: {
        fontSize: 20,
        color: 'black',
        marginTop:5,
    },
    inputAndroid: {
        fontSize: 20,
        color: 'black',
        marginTop:5,
    },
})

function mapStateToProps(state, props){
    console.log(state.user)
    return{
        user: state.user,
    }
}

export default connect(mapStateToProps)(AddMenuItem)