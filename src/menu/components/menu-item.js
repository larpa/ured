import React from 'react'
import {
    View,
    Text,
    StyleSheet,
} from 'react-native'

function MenuItem(props){
    return(
        <View
            style={styles.container}
        >
            <View
                style={styles.titulo}
            >
                <Text
                    style={styles.titleLabel}
                >
                    {props.menu}
                </Text>
                <Text
                    style={styles.titleLabel}
                >
                    S./ {Number(props.precio).toFixed(2)}
                </Text>
            </View>
            <View
                style={styles.tipoContainer}
            >
                {
                    props.tipos.map((tipo, tipoKey) => {
                        return(
                            <View
                                key={tipoKey}
                                style={styles.tipoBox}
                            >
                                <Text
                                    style={styles.tipoTitle}
                                >
                                    {tipo.tipo}
                                </Text>
                                <View>
                                    {
                                        tipo.items.map((items, itemsKey) => {
                                            return(
                                                <View
                                                    key={itemsKey}
                                                    style={styles.itemBox}
                                                >
                                                    <Text
                                                        style={styles.itemText}
                                                    >
                                                        {items.item_menu}
                                                    </Text>
                                                </View>
                                            )
                                        })
                                    }
                                </View>
                            </View>
                        )
                    })
                }
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        marginLeft: 15,
        marginRight: 15,
        marginTop: 10,
        marginBottom: 20,
    },
    titulo:{
        borderBottomWidth: 2,
        borderColor: 'black',
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    titleLabel:{
        margin: 5,
        fontSize: 20,
    },
    tipoContainer: {
        marginLeft: 10,
        marginRight: 10,
    },
    tipoBox:{
        marginTop: 20,
    },
    tipoTitle:{
        fontSize: 18,
    },
    itemBox:{
        backgroundColor: '#FFFFFF',
        borderRadius: 5,
        shadowOffset:{
            width: 0,
            height: 0,
        },
        shadowColor: 'black',
        shadowOpacity: 0.4,
        elevation: 1,
        marginTop: 20,
    },
    itemText:{
        fontSize: 16,
        margin: 10,
    }
})

export default MenuItem