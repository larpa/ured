import React from 'react'
import {
    View,
    Text,
    StyleSheet
} from 'react-native'

function EmptyMenuList(props){
    return(
        <View style={styles.container}>
            <Text style={styles.text}>
                {props.text}
            </Text>
        </View>
    )
}

const styles = StyleSheet.create({
    container:{
        marginTop: 50,
        padding: 20,
    },
    text:{
        fontSize: 22,
        color: '#999999',
        fontWeight: '300',
    }
})

export default EmptyMenuList