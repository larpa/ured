import React, { Component } from 'react'
import {
    SafeAreaView,
    RefreshControl,
    View,
    Text,
    FlatList,
    StyleSheet
} from 'react-native'

import {connect} from 'react-redux'

import { NavigationActions } from 'react-navigation'

import Header from '../../components/header'

import ErrorItem from '../components/error'
import EmptyErrorList from '../components/empty-error-list'

import API from '../../../utils/api'
import showAlert from '../../../utils/alert'

class ErrorList extends Component{

    state = {
        refreshing: false
    }

    keyExtractor = item => item.id.toString()

    renderEmptyComponent = () => <EmptyErrorList
                                    text="Aún no hay Quejas."
                                />

    renderItem = ({item}) =>    <ErrorItem
                                    {...item}
                                />

    renderLastItem = () => <View style={{ height: 0, marginBottom: 150}} ></View>

    handleOnBack = () => {
        this.props.dispatch(NavigationActions.back())
    }

    componentDidMount = () => {
        this.handleListData()
    }

    handleRefresh = () => {
        this.handleListData()
    }

    handleListData = async () => {
        this.setState({
            refreshing: true
        })

        const result = await API.getQuejas()

        console.log(result.response)

        if(result.success){
            this.props.dispatch({
                type: 'LOAD_QUEJAS_LIST_DATA',
                payload: {
                    quejas: result.response
                }
            })
        }else{
            showAlert("Ocurrio un error al cargar las quejas.", 'error', 'long')
        }
        this.setState({
            refreshing: false
        })
    }

    render(){
        return(
            <SafeAreaView>
                <View
                    style={styles.container}
                >
                    <Header
                        title="Quejas"
                        back
                        onPress={this.handleOnBack}
                    />

                    <FlatList
                        style={styles.list}
                        keyExtractor={this.keyExtractor}
                        data={this.props.quejas}
                        ListEmptyComponent={this.renderEmptyComponent}
                        renderItem={this.renderItem}
                        ListFooterComponent={this.renderLastItem}
                        refreshControl={<RefreshControl refreshing={this.state.refreshing} onRefresh={this.handleRefresh}/>}
                    />
                </View>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        height: '100%',
    },
    list:{
        height: '100%'
    },
})

function mapStateToProps(state, props){
    console.log("Hey! ", state.quejas.quejasList)
    return{
        user: state.user,
        quejas: state.quejas.quejasList
    }
}

export default connect(mapStateToProps)(ErrorList)