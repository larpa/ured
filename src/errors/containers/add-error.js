import React, { Component } from 'react'
import {
    View,
    Text,
    SafeAreaView,
    StyleSheet
} from 'react-native'

import { connect } from 'react-redux'

import { NavigationActions } from 'react-navigation'

import Header from '../../components/header'
import TextInput from '../../components/text-input'
import Button from '../../components/button'

import API from '../../../utils/api'
import showAlert from '../../../utils/alert'

class ErrorAdd extends Component{
    state ={
        descripcion: '',
        loading: false,
    }

    handleOnBack = () => {
        this.props.dispatch(NavigationActions.back())
    }

    handleOnChangeEntrada = value => {
        this.setState({
            descripcion: value
        })
    }

    handleRegisterMenuItemButton = async () => {
        this.setState({
            loading: true
        })

        if(this.state.descripcion === ''){
            showAlert('Debes escribir algo.', 'warning', 'long')
            return
        }

        const result = await API.addQueja(this.props.user.id, this.state.descripcion)

        if(result.success){
            showAlert('Se registro tu queja.', 'success', 'long')
            this.props.dispatch(NavigationActions.back())
        }else{
            showAlert('Ocurrio un error al registrar la queja.', 'error', 'long')
        }

        this.setState({
            loading: false
        })
    }

    render(){
        return(
            <SafeAreaView>
                <View
                    style={styles.container}
                >
                    <Header
                        title="Nueva queja"
                        back
                        onPress={this.handleOnBack}
                    />
                    <Text
                        style={styles.indicacion}
                    >
                        Aquí puedes expresar todo lo que tu creas necesario. En ningun lugar aparecerá tu nombre ni se te vinculará con lo que escribas aquí pero ten en cuenta que hay una persona encargada de leer tus quejas. Te recomendamos que describas a detalle lo que quieres expresar.
                    </Text>
                    <TextInput
                        style={styles.input}
                        onChangeText={this.handleOnChangeEntrada}
                        value={this.state.descripcion}
                        multiline={true}
                        numberOfLines={3}
                        returnKeyType="done"
                    />
                    <Button
                        component
                        color='#4A3DDD'
                        onPress={this.handleRegisterMenuItemButton}
                        loading={this.state.loading}
                    >
                        <View
                            style={styles.buttonBox}
                        >
                            <Text
                                style={styles.buttonText}
                            >
                                Registrar
                            </Text>
                        </View>
                    </Button>
                </View>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    input:{
        marginTop: 15,
        marginLeft: 10,
        marginRight: 10,
    },
    indicacion:{
        fontSize: 18,
        color: '#514E4A',
        fontWeight: '300',
        marginLeft: 20,
        marginRight: 20,
        marginTop: 25,
    },
    buttonBox:{
		width: '80%',
        marginTop: 30,
        marginLeft: '10%',
        marginBottom: 10,
		padding: 10,
		alignItems: 'center',
		borderRadius: 10,
		backgroundColor: '#4A3DDD',
    },
    buttonText:{
		fontSize: 18,
		color: 'white',
		fontWeight: '600',
    },
})

function mapStateToProps(state, props){
    return{
        user: state.user
    }
}

export default connect(mapStateToProps)(ErrorAdd)