import React from 'react'
import {
    View,
    Text,
    StyleSheet
} from 'react-native'

import Avatar from '../../components/avatar'

function ErrorItem(props){
    return(
        <View
            style={styles.container}
        >
            <View
                style={styles.header}
            >
                {
                    props.author &&
                    <Avatar 
                        size="small"
                        rounded
                        title={props.author.slice(0,2)}
                        source={{uri: props.authorPhoto}}
                    />
                }
                <View>
                    <Text
                        style={styles.nombre}
                    >
                        {props.author}
                    </Text>
                    <Text
                        style={styles.cargo}
                    >
                        {props.authorArea}
                    </Text>

                </View>
            </View>
            <Text
                style={styles.queja}
            >
                {props.queja}
            </Text>
        </View>
    )
}

const styles = StyleSheet.create({
    container:{
        borderRadius: 5,
        shadowOffset:{
            width: 0,
            height: 0,
        },
        shadowColor: 'black',
        shadowOpacity: 0.4,
        elevation: 1,
        marginTop: 20,
        backgroundColor: 'white',
        marginLeft: 20,
        marginRight: 20,
        padding: 10
    },
    queja:{
        fontSize: 16,
        marginLeft: 14,
        color: '#514E4A',
    },
    header:{
        flexDirection: 'row',
        alignItems: 'center',
        width: '100%',
        paddingLeft: 5,
        paddingRight: 5,
        paddingTop: 5,
        marginBottom: 15,
    },
    nombre:{
        fontSize: 18,
        marginLeft: 10,
    },
    cargo:{
        fontSize: 12,
        color: '#757779',
        marginLeft: 10,
    },
})

export default ErrorItem