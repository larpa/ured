const initialState = {
    colaboradoresList: [],
}

function colaboradoresMes(state = initialState, action){
    switch(action.type){
        case 'LOAD_COLABORADORES_LIST_DATA':
            return {...state, colaboradoresList: action.payload.colaboradores}
        break

        case 'CLEAR_COLABORADORES':
            return {}
        break

        default:
            return state
        break
    }
}

export default colaboradoresMes