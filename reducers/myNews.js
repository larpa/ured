const initialState = {
    myNewsList: null,
    myNewSelected: {},
}
function myNews(state = initialState, action){
    switch(action.type){
        case 'LOAD_MY_NEWS_DATA':
            return {...state, myNewsList: action.payload.news}
        break
        case 'LIKE_MY_NEW':
            let holderliked = [];
            state.myNewsList.map((item, i) => {
                if(item.id === action.payload.newUpdated.id){
                    holderliked.push(action.payload.newUpdated)
                }else{
                    holderliked.push(item)
                }
            })
            return {...state, myNewsList: holderliked}
        break
        case 'SELECT_MY_NEW_LIST':
            const holder = state.myNewsList.find(obj => obj.id === action.payload.id)
            return {...state, myNewSelected: holder}
        break
        case 'CLEAR_MY_NEWS':
            return {}
        break
        default:
            return state
        break
    }
}

export default myNews