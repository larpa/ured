const initialState = {
    contactosList: [],
}

function contactos(state = initialState, action){
    switch(action.type){
        case 'LOAD_CONTACTOS_LIST_DATA':
            return {...state, contactosList: action.payload.contactos}
        break

        case 'CLEAR_CONTACTOS':
            return {}
        break

        default:
            return state
        break
    }
}

export default contactos