import { combineReducers } from 'redux'

import user from './user'
import news from './news'
import notification from './notification'
import navigation from './navigation'
import myNews from './myNews'
import menu from './menu'
import solicitudes from './solicitudes'
import colaboradoresMes from './colaboradorMes'
import contactos from './contactos'
import quejas from './quejas'

const reducer = combineReducers({
    user,
    news,
    notification,
    navigation,
    myNews,
    menu,
    colaboradoresMes,
    contactos,
    quejas,
    solicitudes,
})

export default reducer