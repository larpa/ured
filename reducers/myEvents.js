import { min } from "moment";

const initialState = {
    myEventsList: null,
    permissionList: null,
    assistantsList: null,
    eventSelected: 0,
}

function myEvents(state = initialState, action){
    switch(action.type){
        case 'LOAD_MY_EVENTS_DATA':
            return {...state, myEventsList: action.payload.event}
        break
        
        case 'LOAD_MY_PERMISSIONS_LIST_DATA':
            return {...state, permissionList: action.payload.permissions}
        break

        case 'LOAD_ASSISTANTS':
            return {...state, assistantsList: action.payload.assistants}
        break
        
        case 'SELECT_EVENT':
            return {...state, eventSelected: action.payload.id}
        break
        
        case 'CLEAR_MY_EVENT':
            return {}
        break

        default:
            return state
        break
    }
}

export default myEvents