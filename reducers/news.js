const initialState = {
    newsList: null,
    newSelected: {},
}
function news(state = initialState, action){
    switch(action.type){
        case 'LOAD_NEWS_DATA':
            return {...state, newsList: action.payload.news}
        break
        case 'LIKE_NEW':
            let holderliked = [];
            state.newsList.map((item, i) => {
                if(item.id === action.payload.newUpdated.id){
                    holderliked.push(action.payload.newUpdated)
                }else{
                    holderliked.push(item)
                }
            })
            return {...state, newsList: holderliked}
        break
        case 'SELECT_NEW_LIST':
            const holder = state.newsList.find(obj => obj.id === action.payload.id)
            return {...state, newSelected: holder}
        break

        case 'CLEAR_NEWS':
            return {}
        break
        default:
            return state
        break
    }
}

export default news