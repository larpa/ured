function user(state = null, action){
    switch(action.type){
        case 'SET_USER':
            return {...action.payload.user}
        break
        case 'LOG_OUT':
            return null
        break
        default:
            return state
        break
    }
}

export default user