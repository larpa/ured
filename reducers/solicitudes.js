import { min } from "moment";

const initialState = {
    misSolicitudesList: [],
    solicitudesList: [],
    solicitudSelected: {},
    miSolicitudSelected: {},
}

function misSolicitudes(state = initialState, action){
    switch(action.type){
        case 'LOAD_MIS_SOLICITUDES_DATA':
            return {...state, misSolicitudesList: action.payload.solicitudes}
        break

        case 'LOAD_SOLICITUDES_DATA':
            return {...state, solicitudesList: action.payload.solicitudes}
        break

        case 'SELECT_SOLICITUD':
            let holderOne = state.solicitudesList.find(obj => obj.id === action.payload.id)
            return {...state, solicitudSelected: holderOne}
        break

        case 'SELECT_MI_SOLICITUD':
            let holderTwo = state.misSolicitudesList.find(obj => obj.id === action.payload.id)
            return {...state, miSolicitudSelected: holderTwo}
        break

        case 'CLEAR_SOLICITUDES':
            return {}
        break

        default:
            return state
        break
    }
}

export default misSolicitudes