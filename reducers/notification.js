const initialState = {
    notificationList: []
}
function notification(state = initialState, action){
    switch(action.type){
        case 'LOAD_NOTIFICATIONS':
            return {...state, notificationList: action.payload.notifications}
        break
        case 'CLEAR_NOTIFICATION':
            return {}
        break
        default:
            return state
        break
    }
}

export default notification