const initialState = {
    menuList: null,
}
function Menu(state = initialState, action){
    switch(action.type){
        case 'LOAD_MENU_DATA':
            return {...state, menuList: action.payload.menus}
        break
        case 'CLEAR_MENUS':
            return {}
        break
        default:
            return state
        break
    }
}

export default Menu