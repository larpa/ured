const initialState = {
    quejasList: [],
}

function quejas(state = initialState, action){
    switch(action.type){
        case 'LOAD_QUEJAS_LIST_DATA':
            return {...state, quejasList: action.payload.quejas}
        break

        case 'CLEAR_QUEJAS':
            return {}
        break

        default:
            return state
        break
    }
}

export default quejas