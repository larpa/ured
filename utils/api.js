import { sha256 } from 'js-sha256'

const BASE_API = 'https://e5882e50.ngrok.io/api/'

class Api{
    async login(mail, password){
        try{
            const userData = await fetch(
                `${BASE_API}user/login`,
                {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                    },
                    body: `correo=${mail}&clave=${sha256(password).toUpperCase()}`
                }
            )
            const responseJson = await userData.json()
            if(responseJson.success){
                return {success: true, response: responseJson.data}
            }else{
                return {success: false, response: responseJson.message}
            }
        }catch(e){
            console.log("ERROR... : ", e)
            return {success: false, response: "El servidor no responde"}
        }
    }

    async requestNews(userId, viewerUserId){
        try{
            const newsData = await fetch(
                `${BASE_API}news/last-news`,
                {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                    },
                    body: `userId=${userId}&viewingUserID=${viewerUserId}`
                }
            )
            const responseJson = await newsData.json()
            if(responseJson.success){
                return {success: true, response: responseJson.data}
            }else{
                return {success: false, response: responseJson.message}
            }
        }catch(e){
            console.log("ERROR... : ", e)
            return {success: false, response: "El servidor no responde"}
        }
    }

    async registerNew(userId, content, tipo){
        try{
            const newsData = await fetch(
                `${BASE_API}news/share-publication`,
                {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                    },
                    body: `userId=${userId}&contenido=${content}&tipo=${tipo}`
                }// userId, contenido, tipo
            )
            const responseJson = await newsData.json()

            if(responseJson.success){
                return {success: true, response: responseJson.data}
            }else{
                return {success: false, response: responseJson.message}
            }
        }catch(e){
            console.log("ERROR... : ", e)
            return {success: false, response: "El servidor no responde"}
        }
    }

    async registerNewWithPhoto(userId, content, tipo, files){
        try{
            var formData = new FormData();

            formData.append('userId', userId)
            formData.append('contenido', content)
            formData.append('tipo', tipo)

            files.map((item, i) => {
                formData.append('photos-' + i, {
                    uri: item,
                    type: 'jpeg',
                    name: 'no-name-yet'
                });
            })

            const newsData = await fetch(
                `${BASE_API}news/share-publication-photo`,
                {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'image/jpeg',
                    },
                    body: formData
                }
            )
            const responseJson = await newsData.json()
            if(responseJson.success){
                return {success: true, response: responseJson.data}
            }else{
                return {success: false, response: responseJson.message}
            }
        }catch(e){
            console.log("ERROR... : ", e)
            return {success: false, response: "El servidor no responde"}
        }
    }

    async likeNew(pub_id, user_id){
        try{
            const likeRequest = await fetch(
                `${BASE_API}news/like-publication`,
                {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                    },
                    body: `userId=${user_id}&publicationId=${pub_id}`
                }
            )
            const responseJson = await likeRequest.json()
            if(responseJson.success){
                return {success: true, response: responseJson.data}
            }else{
                return {success: false, response: responseJson.message}
            }
        }catch(e){
            console.log("ERROR... : ", e)
            return {success: false, response: "El servidor no responde"}
        }
    }

    async getTodaysMenu(){
        try{
            const menuRequest = await fetch(
                `${BASE_API}menu/list-todays-menu`,
                {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                    }
                }
            )

            const responseJson = await menuRequest.json()

            if(menuRequest.ok){
                return {success: true, response: responseJson}
            }else{
                return {success: false, response: ''}
            }
        }catch(e){
            console.log("ERROR... : ", e)
            return {success: false, response: "El servidor no responde"}
        }
    }

    async getColaboradoresMes(){
        try{
            const colabRequest = await fetch(
                `${BASE_API}colaborador-mes/list-all`,
                {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                    }
                }
            )

            const responseJson = await colabRequest.json()

            if(colabRequest.ok){
                return {success: true, response: responseJson}
            }else{
                return {success: false, response: ''}
            }
        }catch(e){
            console.log("ERROR... : ", e)
            return {success: false, response: "El servidor no responde"}
        }
    }

    async getContactos(){
        try{
            const contactosRequest = await fetch(
                `${BASE_API}contactos/list-all`,
                {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                    }
                }
            )

            const responseJson = await contactosRequest.json()

            if(contactosRequest.ok){
                return {success: true, response: responseJson}
            }else{
                return {success: false, response: ''}
            }
        }catch(e){
            console.log("ERROR... : ", e)
            return {success: false, response: "El servidor no responde"}
        }
    }

    async getColaboradores(){
        try{
            const colabRequest = await fetch(
                `${BASE_API}colaborador-mes/list-colaboradores`,
                {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                    }
                }
            )

            const responseJson = await colabRequest.json()

            if(colabRequest.ok){
                return {success: true, response: responseJson}
            }else{
                return {success: false, response: ''}
            }
        }catch(e){
            console.log("ERROR... : ", e)
            return {success: false, response: "El servidor no responde"}
        }
    }

    async getQuejas(){
        try{
            const colabRequest = await fetch(
                `${BASE_API}quejas/list-all`,
                {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                    }
                }
            )

            const responseJson = await colabRequest.json()

            if(colabRequest.ok){
                return {success: true, response: responseJson}
            }else{
                return {success: false, response: ''}
            }
        }catch(e){
            console.log("ERROR... : ", e)
            return {success: false, response: "El servidor no responde"}
        }
    }

    async addQueja(id_perfil, descripcion){
        try{
            const menuRequest = await fetch(
                `${BASE_API}quejas/add-queja`,
                {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                    },
                    body: `id_perfil=${id_perfil}&descripcion=${descripcion}`
                }
            )
            const responseJson = await menuRequest.json()
            if(menuRequest.ok || responseJson){
                return {success: true, response: responseJson}
            }else{
                return {success: false, response: ''}
            }
        }catch(e){
            console.log("ERROR... : ", e)
            return {success: false, response: "El servidor no responde"}
        }
    }

    async getMenus(){
        try{
            const menuRequest = await fetch(
                `${BASE_API}menu/list-menus`,
                {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                    }
                }
            )
            const responseJson = await menuRequest.json()
            if(menuRequest.ok || responseJson){
                return {success: true, response: responseJson}
            }else{
                return {success: false, response: ''}
            }
        }catch(e){
            console.log("ERROR... : ", e)
            return {success: false, response: "El servidor no responde"}
        }
    }

    async getTipoSolicitud(){
        try{
            const menuRequest = await fetch(
                `${BASE_API}solicitud/list-tipo-solicitudes`,
                {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                    }
                }
            )
            const responseJson = await menuRequest.json()
            if(menuRequest.ok || responseJson){
                return {success: true, response: responseJson}
            }else{
                return {success: false, response: ''}
            }
        }catch(e){
            console.log("ERROR... : ", e)
            return {success: false, response: "El servidor no responde"}
        }
    }

    async getMisSolicitudes(id_perfil){
        try{
            const menuRequest = await fetch(
                `${BASE_API}solicitud/list-mis-solicitudes`,
                {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                    },
                    body: `id_perfil=${id_perfil}`
                }
            )
            const responseJson = await menuRequest.json()
            if(menuRequest.ok || responseJson){
                return {success: true, response: responseJson}
            }else{
                return {success: false, response: ''}
            }
        }catch(e){
            console.log("ERROR... : ", e)
            return {success: false, response: "El servidor no responde"}
        }
    }

    async getSolicitudes(id_perfil){
        try{
            const menuRequest = await fetch(
                `${BASE_API}solicitud/list-solicitudes`,
                {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                    },
                    body: `id_perfil=${id_perfil}`
                }
            )
            const responseJson = await menuRequest.json()
            if(menuRequest.ok || responseJson){
                return {success: true, response: responseJson}
            }else{
                return {success: false, response: ''}
            }
        }catch(e){
            console.log("ERROR... : ", e)
            return {success: false, response: "El servidor no responde"}
        }
    }

    async getSolicitudesPersonal(){
        try{
            const menuRequest = await fetch(
                `${BASE_API}solicitud/list-solicitudes-personal`,
                {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                    }
                }
            )
            const responseJson = await menuRequest.json()
            console.log(responseJson)
            if(menuRequest.ok || responseJson){
                return {success: true, response: responseJson}
            }else{
                return {success: false, response: ''}
            }
        }catch(e){
            console.log("ERROR... : ", e)
            return {success: false, response: "El servidor no responde"}
        }
    }

    async changeEstadoSolicitud(id_solicitud, observacion, fecha_desde, fecha_hasta, hora_desde, hora_hasta, estado, id_perfil){
        try{
            const changeStatusRequest = await fetch(
                `${BASE_API}solicitud/estado-solicitud`,
                {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                    },
                    body: `id_perfil=${id_perfil}&id_solicitud=${id_solicitud}&observacion=${observacion}&fecha_desde=${fecha_desde}&fecha_hasta=${fecha_hasta}&hora_desde=${hora_desde}&hora_hasta=${hora_hasta}&estado=${estado}`
                }
            )
            const responseJson = await changeStatusRequest.json()
            if(changeStatusRequest.ok || responseJson){
                return {success: true, response: ''}
            }else{
                return {success: false, response: ''}
            }
        }catch(e){
            console.log("ERROR... : ", e)
            return {success: false, response: "El servidor no responde"}
        }
    }

    async getAdjuntos(id_solicitud){
        try{
            const menuRequest = await fetch(
                `${BASE_API}solicitud/list-adjuntos`,
                {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                    },
                    body: `id_solicitud=${id_solicitud}`
                }
            )
            const responseJson = await menuRequest.json()
            if(menuRequest.ok && responseJson){
                return {success: true, response: responseJson}
            }else{
                return {success: false, response: ''}
            }
        }catch(e){
            console.log("ERROR... : ", e)
            return {success: false, response: "El servidor no responde"}
        }
    }

    async getObservaciones(id_solicitud){
        try{
            const observacionesRequest = await fetch(
                `${BASE_API}solicitud/list-observaciones`,
                {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                    },
                    body: `id_solicitud=${id_solicitud}`
                }
            )
            const responseJson = await observacionesRequest.json()
            console.log("observaciones: ", responseJson)
            if(observacionesRequest.ok && responseJson){
                return {success: true, response: responseJson}
            }else{
                return {success: false, response: ''}
            }
        }catch(e){
            console.log("ERROR... : ", e)
            return {success: false, response: "El servidor no responde"}
        }
    }

    async addSolicitud(id_perfil, id_tipo_solicitud, urgente, fecha_desde, fecha_hasta, hora_desde, hora_hasta, motivo, adjuntos){
        try{
            var formData = new FormData();

            formData.append('id_perfil', id_perfil)
            formData.append('id_tipo_solicitud', id_tipo_solicitud)
            formData.append('urgente', urgente)
            formData.append('fecha_desde', fecha_desde)
            formData.append('fecha_hasta', fecha_hasta)
            formData.append('hora_desde', hora_desde)
            formData.append('hora_hasta', hora_hasta)
            formData.append('motivo', motivo)

            adjuntos.map((item, i) => {
                formData.append('photos-' + i, {
                    uri: item,
                    type: 'jpeg',
                    name: 'no-name-yet'
                });
            })

            const addRequest = await fetch(
                `${BASE_API}solicitud/add-solicitud`,
                {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'image/jpeg',
                    },
                    body: formData
                }
            )

            const responseJson = await addRequest.json()

            if(addRequest.ok){
                return {success: true}
            }else{
                return {success: false}
            }
        }catch(e){
            console.log("ERROR... : ", e)
            return {success: false, response: "El servidor no responde"}
        }
    }

    async changeProfilePic(id_perfil, photo){
        try{
            var formData = new FormData();

            formData.append('id_perfil', id_perfil)
            formData.append('photo', {
                uri: photo,
                type: 'jpeg',
                name: 'no-name-yet'
            })

            const addRequest = await fetch(
                `${BASE_API}user/change-profile-pic`,
                {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'image/jpeg',
                    },
                    body: formData
                }
            )

            const responseJson = await addRequest.json()

            if(addRequest.ok){
                return {success: true, response: responseJson.response}
            }else{
                return {success: false}
            }
        }catch(e){
            console.log("ERROR... : ", e)
            return {success: false, response: "El servidor no responde"}
        }
    }

    async addColaboradorMes(id_perfil, fecha, observacion){
        try{
            const addRequest = await fetch(
                `${BASE_API}colaborador-mes/add-colaborador`,
                {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                    },
                    body: `id_perfil=${id_perfil}&fecha=${fecha}&observacion=${observacion}`
                }
            )

            const responseJson = await addRequest.json()

            if(addRequest.ok || responseJson){
                return {success: true, response: responseJson}
            }else{
                return {success: false}
            }
        }catch(e){
            console.log("ERROR... : ", e)
            return {success: false, response: "El servidor no responde"}
        }
    }

    async addMenu(nombre, precio, user_id){
        try{
            const addRequest = await fetch(
                `${BASE_API}menu/add-menu`,
                {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                    },
                    body: `userId=${user_id}&nombre=${nombre}&precio=${precio}`
                }
            )

            const responseJson = await addRequest.json()

            if(addRequest.ok || responseJson){
                return {success: true, response: responseJson}
            }else{
                return {success: false}
            }
        }catch(e){
            console.log("ERROR... : ", e)
            return {success: false, response: "El servidor no responde"}
        }
    }

    async addItemMenu(menu_id, entrada, segundo, bebida, postre, user_id){
        try{
            const addRequest = await fetch(
                `${BASE_API}menu/add-menu-item`,
                {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                    },
                    body: `userId=${user_id}&menu_id=${menu_id}&entrada=${entrada}&segundo=${segundo}&bebida=${bebida}&postre=${postre}`
                }
            )

            const responseJson = await addRequest.json()

            if(addRequest.ok){
                return {success: true}
            }else{
                return {success: false, message: responseJson.message}
            }
        }catch(e){
            console.log("ERROR... : ", e)
            return {success: false, response: "El servidor no responde"}
        }
    }
}

export default new Api()