import React from 'react'

import Snackbar from 'react-native-snackbar'

function showAlert(title, type, duration, action){
    // tipos de alerta predefinidos
    // type -> error | message | warning 
    // ---------------------------------
    // prototipo de objeto para acciones
    // action -> {
    //     title: 'UNDO',
    //     color: 'green',
    //     onPress: () => { /* Do something. */ },
    //   }
    // ---------------------------------
    // duration -> long | short
    
    if(!action){
        action = {
            title: '',
            color: '#00ff001C'
        }
    }
    if(duration){
        if(duration === "long"){
            duration = Snackbar.LENGTH_LONG
        }else{
            duration = Snackbar.LENGTH_SHORT
        }
    }else{
        duration = Snackbar.LENGTH_INDEFINITE
    }
    let bg_color = ''
    let color = ''
    if(type === 'error'){
        bg_color = '#FF2C2D'
        color = '#1C1C1C'
    }else if(type === 'warning'){
        bg_color = '#FFD838'
        color = '#1C1C1C'
    }else{ // message
        bg_color = '#1C1C1C'
        color = '#D7D7D7'
    }

    Snackbar.show({
        title,
        duration,
        backgroundColor: bg_color,
        color,
        action
    })
}

export default showAlert