import React from 'react'
import ImagePicker from 'react-native-image-picker'

// THIS UTILITIE IS TO GET IMAGES FROM GALERY

function imagePicker(allowsEditing){
    return new Promise((resolve, reject)=> {
        ImagePicker.launchImageLibrary({
            mediaType: 'photo',
            allowsEditing,
            noData: true,
            storageOptions: {
              skipBackup: false,
              path: 'images',
            },
          }, async (response) => {
            if (response.error) {
                reject(response.error)
            }else{
                resolve(response)
            }
        });
    })
}

export default imagePicker