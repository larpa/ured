import React from 'react'
import ReactNativeHapticFeedback from 'react-native-haptic-feedback';

// Possible values are 
// "selection", 
// "impactLight", 
// "impactMedium", 
// "impactHeavy", 
// "notificationSuccess", 
// "notificationWarning", 
// "notificationError" 
// (default: "selection")

function HapticFeedback(impact){
    const options = {
        enableVibrateFallback: true,
        ignoreAndroidSystemSettings: false
    }

    ReactNativeHapticFeedback.trigger(impact, options)
}

export default HapticFeedback